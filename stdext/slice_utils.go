package stdext

// MakeBoolSlice allows specifying a default value when making a bool slice.
func MakeBoolSlice(size int, initialValue bool) []bool {
	result := make([]bool, size)
	for i := range result {
		result[i] = initialValue
	}
	return result
}

// MakeIntSlice allows specifying a default value when making an int slice.
func MakeIntSlice(size int, initialValue int) []int {
	result := make([]int, size)
	for i := range result {
		result[i] = initialValue
	}
	return result
}

// MakeBoolMatrix creates a bool matrix and initializes with the given initial value.
func MakeBoolMatrix(n, m int, initialValue bool) [][]bool {
	result := make([][]bool, n)
	for i := 0; i < n; i++ {
		result[i] = make([]bool, m)
		for j := 0; j < m; j++ {
			result[i][j] = initialValue
		}
	}
	return result
}

// MakeIntMatrix creates an int matrix and initializes with the given initial value.
func MakeIntMatrix(n, m int, initialValue int) [][]int {
	result := make([][]int, n)
	for i := 0; i < n; i++ {
		result[i] = make([]int, m)
		for j := 0; j < m; j++ {
			result[i][j] = initialValue
		}
	}
	return result
}
