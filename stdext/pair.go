package stdext

type Pair[T, U any] struct {
	First  T
	Second U
}

type IntPair Pair[int, int]
