package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sql.Open("sqlite3", "file:users.db?mode=rw")
	if err != nil {
		log.Fatal(err)
	}

	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(50)

	pingDatabase(db)

	users := findUsers(db)

	for _, user := range users {
		fmt.Println(user)
	}

}

func findUsers(db *sql.DB) []User {
	rows, err := db.Query("select * from Users")
	if err != nil {
		log.Fatal(err)
	}

	var users []User
	for rows.Next() {
		var user User

		err = rows.Scan(&user.ID, &user.Login, &user.Password)
		if err != nil {
			log.Fatal(err)
		}

		users = append(users, user)
	}

	return users
}

func pingDatabase(db *sql.DB) {
	err := db.Ping()
	if err != nil {
		log.Fatal(err)
	}
}
