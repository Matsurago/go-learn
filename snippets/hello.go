package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	rt "runtime" // renaming import
)

// this runs when a package is loaded
func init() {
	fmt.Println("Hello, GoLang!")
}

func main() {
	log.Printf("Running on '%v'.\n", rt.GOOS)
	defer fmt.Println("This will be printed when surrounding function, main(), exits.")
	defer fmt.Println("Deferred statements are executed according to LIFO principle.")

	// first arg is the full path to the app (dir + filename)
	fmt.Println("The args are:", os.Args[1:])

	// flag package provides convenient functions to work with command-line arguments
	// NOTE: returns pointers
	outputPath := flag.String("o", "default.txt", "Output filename") // flag, default value, help message
	showVersion := flag.Bool("v", false, "Show app version")

	flag.Parse() // parses and validates args

	fmt.Printf("Output path is '%v'.\n", *outputPath)
	fmt.Printf("Show version flag is '%v'.\n", *showVersion)

	log.Println("Exiting main().")
}
