package main

import (
	"math/rand"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

func TestRandom(t *testing.T) {
	seed := time.Now().Unix()
	rng := rand.New(rand.NewSource(seed))

	// get an int in [0, 100)
	randomValue := rng.Intn(100)

	True(t, randomValue > 0 && randomValue < 100)
}
