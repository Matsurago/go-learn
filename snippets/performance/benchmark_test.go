package performance

import (
	"testing"
)

func factorialRecursive(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorialRecursive(n-1)
}

func factorialLoop(n int) int {
	result := 1
	for i := 1; i <= n; i++ {
		result *= i
	}
	return result
}

func BenchmarkFactorialRecursive(b *testing.B) {
	for i := 1; i <= b.N; i++ {
		factorialRecursive(20)
	}
}

func BenchmarkFactorialLoop(b *testing.B) {
	for i := 1; i <= b.N; i++ {
		factorialLoop(20)
	}
}
