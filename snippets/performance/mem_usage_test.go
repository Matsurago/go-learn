package performance

import (
	"fmt"
	"runtime"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func memConsumed() uint64 {
	runtime.GC() // run the GC

	var s runtime.MemStats
	runtime.ReadMemStats(&s)

	return s.Sys
}

func TestMemUsage(t *testing.T) {
	before := memConsumed()

	a := make([]int, 10000)
	Equal(t, 0, a[9999])

	after := memConsumed()

	fmt.Printf("Array creation consumed %d Kb\n", (after-before)/1024)
}
