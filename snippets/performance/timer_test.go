package performance

import (
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

func TestTimer(t *testing.T) {
	start := time.Now()

	time.Sleep(time.Nanosecond * 100)

	timeElapsed := time.Since(start).Nanoseconds()

	True(t, timeElapsed >= 100)
}
