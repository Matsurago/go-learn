package main

import (
	"fmt"
	"strconv"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestTypeConversion(t *testing.T) {
	// string -> bool
	b, _ := strconv.ParseBool("true")
	True(t, b)

	// bool -> string
	Equal(t, "true", strconv.FormatBool(true))

	// string -> float
	f, _ := strconv.ParseFloat("1.276", 64) // str, bitsize

	// float -> string
	Equal(t, "1.28", strconv.FormatFloat(f, 'f', 2, 64)) // float, format, precision, bitsize

	// int -> string
	Equal(t, "127", strconv.Itoa(127))

	// string -> int
	i, err := strconv.Atoi("127")
	Equal(t, nil, err)
	Equal(t, 127, i)
}

func TestIntToBinaryString(t *testing.T) {
	i := 126
	result := fmt.Sprintf("%b", i)

	Equal(t, "1111110", result)
}

func TestIntToHexString(t *testing.T) {
	i := 123456789
	result := fmt.Sprintf("%x %#[1]x %#[1]X", i)

	Equal(t, "75bcd15 0x75bcd15 0X75BCD15", result)
}
