package main

import (
	"regexp"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestPartialMatch(t *testing.T) {
	m, _ := regexp.MatchString("\\d{4}", "0123")
	True(t, m)

	m, _ = regexp.MatchString("\\d{4}", "012a")
	False(t, m)
}

func TestPartialMatchCompiled(t *testing.T) {
	pat, err := regexp.Compile("\\d{2}-[a-z]{3}")
	Equal(t, nil, err)

	True(t, pat.MatchString("11-abc"))
	False(t, pat.MatchString("11-999"))
}

func TestFindAll(t *testing.T) {
	pat := regexp.MustCompile("\\d+")

	res := pat.FindAllString("The error codes are 1024, 713, and 19.", -1)

	Equal(t, []string{"1024", "713", "19"}, res)
}

func TestCaptureGroup(t *testing.T) {
	pat := regexp.MustCompile("(\\d{3})-(\\d{4}-\\d{4})")
	res := pat.FindStringSubmatch("777-8888-9999")

	Equal(t, []string{"777-8888-9999", "777", "8888-9999"}, res)
}

func TestNamedCaptureGroup(t *testing.T) {
	pat := regexp.MustCompile("(?P<AREA>\\d{3})-(?P<NUMBER>\\d{4}-\\d{4})")
	res := pat.FindStringSubmatch("777-8888-9999")

	Equal(t, "777", res[pat.SubexpIndex("AREA")])
	Equal(t, "8888-9999", res[pat.SubexpIndex("NUMBER")])
}

func TestRegexReplace(t *testing.T) {
	pat := regexp.MustCompile("\\d+")
	res := pat.ReplaceAllString("1234 test 12 x 1 2", "")

	Equal(t, " test  x  ", res)

	pat = regexp.MustCompile("ID:(\\d+)")
	res = pat.ReplaceAllString("tableware ID:123, shipment ID:9", "${1}")

	Equal(t, "tableware 123, shipment 9", res)
}
