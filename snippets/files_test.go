package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestReadFile(t *testing.T) {
	b, err := os.ReadFile("files_test.go")
	Equal(t, nil, err)

	lines := strings.Split(string(b), "\n")
	Equal(t, "package main", lines[0])

	_, err = os.ReadFile("files_test_xyz.go")
	NotEqual(t, nil, err)
}

func TestReadFileLineByLine(t *testing.T) {
	f, err := os.Open("files_test.go")
	Equal(t, nil, err)

	scanner := bufio.NewScanner(f) // also works with os.Stdin
	for scanner.Scan() {
		line := scanner.Text() // each line in file
		NotEqual(t, nil, line)
	}

	// check for scanner errors
	Equal(t, nil, scanner.Err())

	// close file
	err = f.Close()
	Equal(t, nil, err)
}

func TestEncodeJson(t *testing.T) {
	type Pizza struct {
		Name       string `json:"name,omitempty"` // omit if empty
		Size       int    `json:"size"`
		InternalId uint8  `json:"-"` // do not encode
		chefName   string // private fields are never encoded
	}

	p := Pizza{
		Name: "Marinara",
		Size: 13,
	}

	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf) // can also use json.Marshal and json.MarshalIndent
	err := enc.Encode(p)

	Equal(t, nil, err)
	Equal(t, `{"name":"Marinara","size":13}`+"\n", buf.String())

	p = Pizza{
		Size: 24,
	}

	buf.Truncate(0)
	err = enc.Encode(p)

	Equal(t, `{"size":24}`+"\n", buf.String())
}

func TestDecodeJson(t *testing.T) {
	type Pizza struct {
		Name string
		Size int
	}

	// unmarshalling is case-insensitive
	input := `[{"name": "Marinara", "size": 13}, {"name": "Diablo", "size": 16}]`
	var res []Pizza

	err := json.Unmarshal([]byte(input), &res)
	Equal(t, nil, err)
	Equal(t, 2, len(res))
	Equal(t, "Marinara", res[0].Name)
	Equal(t, 13, res[0].Size)
	Equal(t, "Diablo", res[1].Name)
	Equal(t, 16, res[1].Size)
}

func TestListDir(t *testing.T) {
	files, err := os.ReadDir(".")
	Equal(t, nil, err)

	for _, f := range files {
		if f.IsDir() {
			fmt.Printf("[ %s ]\n", f.Name())
		} else {
			info, err := f.Info()
			Equal(t, nil, err)

			fmt.Printf("%s, size: %d bytes\n", info.Name(), info.Size())
		}
	}
}

func TestListDirRecursively(t *testing.T) {
	// need to define a function to process file entries
	f := func(path string, e fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if e.IsDir() {
			// skip certain directories, e.g. those starting with _
			if strings.HasPrefix(e.Name(), "_") {
				return fs.SkipDir
			}
		} else {
			fmt.Println(e.Name())
		}

		return nil
	}

	err := filepath.WalkDir(".", f)
	Equal(t, nil, err)
}

func TestCreateDir(t *testing.T) {
	err := os.MkdirAll("temp", 0777) // create folders, including nested ones
	Equal(t, nil, err)

	// try to create an existing directory
	err = os.Mkdir("temp", 0777)
	True(t, errors.Is(err, fs.ErrExist))

	// delete the directory and all its contents
	err = os.RemoveAll("temp")
	Equal(t, nil, err)
}

func TestCreateFile(t *testing.T) {
	const fn = "temp.txt"

	f, err := os.Create(fn)
	Equal(t, nil, err)

	_, err = f.Write([]byte("test data"))
	Equal(t, nil, err)

	err = f.Close()
	Equal(t, nil, err)

	err = os.Remove(fn)
	Equal(t, nil, err)
}

func TestGetFileExt(t *testing.T) {
	fn := "files_test.go"
	Equal(t, ".go", filepath.Ext(fn))
}

func TestGetAbsolutePathToCurrentDir(t *testing.T) {
	dir, err := filepath.Abs(".")
	Equal(t, nil, err)

	True(t, strings.HasSuffix(dir, "snippets"))
}

func TestJoinPaths(t *testing.T) {
	res := filepath.Join("home", "user")
	expected := "home" + string(filepath.Separator) + "user"

	Equal(t, expected, res)
}
