package main

import (
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

func TestDateNow(t *testing.T) {
	cur := time.Now()

	True(t, cur.Year() >= 2022)
}

func TestCreateDate(t *testing.T) {
	d := time.Date(2000, time.March, 11, 12, 0, 0, 0, time.Local)
	Equal(t, 2000, d.Year())

	u := time.Unix(100000000, 0) // Unix time
	Equal(t, 1973, u.Year())
}

func TestFormatDate(t *testing.T) {
	d := time.Date(2012, time.March, 11, 12, 0, 0, 0, time.UTC)
	s := d.Format(time.RFC3339)

	Equal(t, "2012-03-11T12:00:00Z", s)

	s = d.Format(time.RFC822)
	Equal(t, "11 Mar 12 12:00 UTC", s)
}

func TestParseDate(t *testing.T) {
	d, err := time.Parse(time.RFC3339, "2012-03-11T12:00:00Z")
	Equal(t, nil, err)

	Equal(t, 2012, d.Year())
	Equal(t, time.March, d.Month())
	Equal(t, 11, d.Day())
}

func TestDateComparison(t *testing.T) {
	d1 := time.Date(2000, time.March, 11, 12, 0, 0, 0, time.Local)
	d2 := time.Date(2000, time.March, 12, 0, 0, 0, 0, time.Local)

	True(t, d1.Before(d2))
	True(t, d2.After(d1))

	False(t, d1 == d2)
	False(t, d1.Equal(d2)) // takes into account time zone
}

func TestDateTruncate(t *testing.T) {
	d := time.Date(2000, time.March, 11, 12, 23, 12, 999, time.Local)
	res := d.Truncate(time.Hour)
	exp := time.Date(2000, time.March, 11, 12, 0, 0, 0, time.Local)

	Equal(t, exp, res)
}

func TestAddDuration(t *testing.T) {
	d := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.Local)
	dur := 1*time.Hour + 30*time.Minute

	d = d.Add(dur)
	exp := time.Date(2000, time.January, 1, 1, 30, 0, 0, time.Local)

	Equal(t, exp, d)
}

func TestDurationApi(t *testing.T) {
	dur := 1*time.Hour + 30*time.Minute

	// pitfall: gives total duration in given units as float64
	Equal(t, 1.5, dur.Hours())
	Equal(t, 90, int(dur.Minutes()))
}

func TestParseDuration(t *testing.T) {
	dur, err := time.ParseDuration("18h15m10s")
	Equal(t, nil, err)
	Equal(t, 18*3600+15*60+10, int(dur.Seconds()))
}
