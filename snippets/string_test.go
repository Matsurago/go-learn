package main

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
	"strings"
	"testing"
	"unicode"

	. "github.com/stretchr/testify/assert"
)

func TestStringBuffer(t *testing.T) {
	var buf bytes.Buffer

	for i := 1; i <= 5; i++ {
		buf.WriteString(strconv.Itoa(i))
	}

	Equal(t, "12345", buf.String())
}

func TestUnicode(t *testing.T) {
	s := "保守性"

	// unicode characters occupy more than 1 byte
	NotEqual(t, 3, len(s)) // len gives length in bytes, not in characters

	// however, range operates on multibyte character level
	var buf bytes.Buffer
	for _, v := range s {
		buf.WriteRune(v)
	}

	Equal(t, s, buf.String())
}

func TestUnicodePitfall(t *testing.T) {
	s1 := "a cat is in the house"

	Equal(t, "cat", s1[2:5])

	// must be careful with Unicode
	s2 := "札幌"
	Equal(t, "札", s2[0:3]) // each character is 3 bytes length
	Equal(t, "札", s2[0:strings.Index(s2, "幌")])
}

// the proper way to get unicode characters out of a unicode string
func TestGetUnicodeCharacter(t *testing.T) {
	s := "€33.33"

	// first cast string to a slice of runes
	runes := []rune(s)
	// next can get the first Unicode character (not the first byte)
	currency := runes[0]
	// then need to cast the rune back to string
	result := string(currency)

	Equal(t, "€", result)
}

func TestBytesToString(t *testing.T) {
	input := []byte{0x41, 0x42, 0x43}

	Equal(t, "ABC", string(input))
	Equal(t, input, []byte("ABC"))
}

func TestRawString(t *testing.T) {
	path := `C:\new\data`

	Equal(t, "C:\\new\\data", path)
}

func TestTrim(t *testing.T) {
	Equal(t, "test data", strings.TrimSpace("   \ttest data\n"))

	Equal(t, "1 2 3", strings.Trim("[1 2 3]", "[]"))
}

func TestContains(t *testing.T) {
	True(t, strings.Contains("Kate", "ate"))
	False(t, strings.Contains("Cat", "ate"))
}

func TestSubstringIndex(t *testing.T) {
	Equal(t, 7, strings.Index("A tiny cat is jumping", "cat"))
	Equal(t, -1, strings.Index("crab", "cake"))
}

func TestIndexByPredicate(t *testing.T) {
	isVowel := func(r rune) bool {
		return r == 'a' || r == 'e' || r == 'i' || r == 'o' || r == 'u'
	}

	s := "bwrhanda hei"

	Equal(t, 4, strings.IndexFunc(s, isVowel))
}

func TestCount(t *testing.T) {
	Equal(t, 2, strings.Count("The are %s of %s.", "%s"))
	Equal(t, 0, strings.Count("test", "f"))
}

func TestStartsEndsWith(t *testing.T) {
	True(t, strings.HasPrefix("https://ya.ru", "https"))
	True(t, strings.HasSuffix("https://ya.ru", ".ru"))
}

func TestReplace(t *testing.T) {
	Equal(t, "bbaa", strings.Replace("aaaa", "a", "b", 2))
	Equal(t, "bbbb", strings.Replace("aaaa", "a", "b", -1))
}

func TestSplit(t *testing.T) {
	expected := []string{"a", "b", "c"}

	Equal(t, expected, strings.Split("a,b,c", ","))
	Equal(t, expected, strings.Fields("a  b   c"))
}

func TestJoin(t *testing.T) {
	input := []string{"a", "b", "c"}

	Equal(t, "a.b.c", strings.Join(input, "."))
}

func TestRepeat(t *testing.T) {
	Equal(t, "*****", strings.Repeat("*", 5))
}

func TestCaseInsensitiveComparison(t *testing.T) {
	True(t, strings.EqualFold("An Example JX", "an Example Jx"))
}

func TestUnicodeCase(t *testing.T) {
	True(t, unicode.IsLower('ω'))
	True(t, unicode.IsUpper('Ω'))
}

func TestStringBuilder(t *testing.T) {
	// use builder for efficient concatenation in a loop
	var b strings.Builder

	for i := 48; i < 64; i++ {
		b.WriteRune(rune(i))
	}

	Equal(t, "0123456789:;<=>?", b.String())
}

func TestParseString(t *testing.T) {
	var s string
	var i int
	var f float64

	input := "test 7 4.44\n"
	_, err := fmt.Sscanln(input, &s, &i, &f)

	Equal(t, nil, err)
	Equal(t, "test", s)
	Equal(t, 7, i)
	True(t, math.Abs(f-4.44) < 1e-6)
}
