package main

import (
	"strings"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestIfCondition(t *testing.T) {
	parse := func() error {
		return nil
	}

	// can limit the scope of e.g. error variable
	if err := parse(); err != nil {
		Fail(t, "expected err != nil")
	}

	// err is not visible here
}

func TestSwitch(t *testing.T) {
	var success int
	var errors int
	a := "OK"

	switch val := strings.ToLower(a); val {
	case "ok":
		success++
	case "error":
		errors++
	}

	Equal(t, 1, success)
	Equal(t, 0, errors)
}

func TestSwitchNoVar(t *testing.T) {
	sign := func(x int) int {
		switch {
		case x < 0:
			return -1
		case x > 0:
			return 1
		default:
			return 0
		}
	}

	Equal(t, 1, sign(42))
}
