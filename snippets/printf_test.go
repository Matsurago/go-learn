package main

import (
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestPrintfBool(t *testing.T) {
	var a bool
	Equal(t, "false", fmt.Sprintf("%t", a))
}

func TestPrintfHex(t *testing.T) {
	a := 255
	Equal(t, "ff", fmt.Sprintf("%x", a))

	c := 'J'
	Equal(t, "4a", fmt.Sprintf("%x", c))

	text := "Alice"
	Equal(t, "416c696365", fmt.Sprintf("%x", text))
}

func TestPrintfType(t *testing.T) {
	a := [3]int{1, 2, 3}
	Equal(t, "[3]int", fmt.Sprintf("%T", a))
}

func TestPrintString(t *testing.T) {
	s := "hello, world"
	Equal(t, "s = hello, world", fmt.Sprintf("s = %s", s))
	Equal(t, "s = \"hello, world\"", fmt.Sprintf("s = %q", s))
}

func TestPrintSpecifyingArgPositions(t *testing.T) {
	s := "hi"
	x := 5
	Equal(t, "5 hi", fmt.Sprintf("%[2]d %[1]s", s, x))

	// can reuse the same argument multiple times
	a := 7.9
	Equal(t, "7.9 (float64)", fmt.Sprintf("%[1]v (%[1]T)", a))
}

func TestStringFormatting(t *testing.T) {
	item := struct {
		name   string
		rating float64
	}{
		name:   "Tesla",
		rating: 7.9275,
	}

	// negative number: align right, positive number: align left
	res := fmt.Sprintf("%-10v: %4.2f pt.", item.name, item.rating)
	Equal(t, "Tesla     : 7.93 pt.", res)
}
