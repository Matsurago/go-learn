package main

import (
	"errors"
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

// unhandled panic can shut down the app
// this example demonstrates how to property recover from panic
func panicExample(i int) (result int, err error) {
	// recover() can be used to prevent crashing app from panic
	// it must be deferred or it has no chance to execute
	defer func() {
		r := recover()
		if r == nil { // panic didn't happen
			return
		}

		if e, ok := r.(error); ok { // panic with error, just return error
			err = e
			return
		}

		err = fmt.Errorf("non-error panic of type '%T' with value '%v'", r, r)
	}()

	switch i {
	case 1:
		result = i * 2
	case 2:
		panic("test") // can panic with ANY object
	case 3:
		panic(errors.New("some error"))
	}

	return i, nil
}

func TestRecover(t *testing.T) {
	var err error

	_, err = panicExample(1)
	Equal(t, nil, err)

	_, err = panicExample(2)
	Equal(t, "non-error panic of type 'string' with value 'test'", err.Error())

	_, err = panicExample(3)
	Equal(t, "some error", err.Error())
}
