package main

import (
	"fmt"
	"log"
	"os"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestLogToFile(t *testing.T) {
	logFilename := os.TempDir() + "gotest.log"
	fmt.Printf("Logging to '%s'.\n", logFilename)

	f, err := os.OpenFile(logFilename, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	Equal(t, nil, err)
	defer f.Close()

	log.SetOutput(f)

	log.Println("Test")
}
