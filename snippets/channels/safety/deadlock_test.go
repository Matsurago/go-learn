package safety

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

// Coffman's Conditions for a deadlock:
//  1. Mutual Exclusion: a concurrent process holds exclusive rights to a resource at any one time.
//  2. Wait For Condition: a concurrent process must simultaniously hold a resource and be waiting for
//     an additional resource.
//  3. No Preemption: a resource held by a concurrent process can only be released by this process.
//  4. Circular Wait: a concurrent process must be waiting on a chain of other concurrent processes,
//     which are in turn waiting on it.
func TestDeadlock(t *testing.T) {
	type value struct {
		sync.Mutex
		x int
	}

	add := func(a *value, b *value, wg *sync.WaitGroup) {
		defer wg.Done()

		a.Lock()
		defer a.Unlock()

		time.Sleep(1 * time.Second)

		b.Lock()
		defer b.Unlock()

		fmt.Printf("a + b = %d\n", a.x+b.x)
	}

	var wg sync.WaitGroup
	wg.Add(2)

	var a, b value
	go add(&a, &b, &wg)
	go add(&b, &a, &wg)

	wg.Wait()
}
