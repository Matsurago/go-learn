package safety

import (
	"fmt"
	"sync"
	"testing"
)

// The program is non-deterministic, there are 3 possible outcomes (a data race).
// 1) i = 0, first if branch is executed
// 2) i = 1, second if branch is executed
// 3) i = 1, first if branch is executed
func TestDataRace(t *testing.T) {
	i := 0

	go func() {
		i++
	}()

	if i == 0 {
		fmt.Printf("(i is zero), i = %d\n", i)
	} else {
		fmt.Printf("i = %d\n", i)
	}
}

// We fixed the data race by synchronizing access to the shared memory.
// However, there is still a race condition: the program is still non-deterministic,
// we don't know which statement (goroutine's increment or if-statement) is executed first.
func TestRaceCondition(t *testing.T) {
	i := 0
	var mu sync.Mutex

	go func() {
		mu.Lock()
		i++
		mu.Unlock()
	}()

	mu.Lock()
	if i == 0 {
		fmt.Printf("(i is zero), i = %d\n", i)
	} else {
		fmt.Printf("i = %d\n", i)
	}
	mu.Unlock()
}
