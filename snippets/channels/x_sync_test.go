package channels

import (
	"context"
	"fmt"
	"golang.org/x/sync/errgroup"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

// errgroup.Group is an improvement over sync.WaitGroup that allows handling errors
// returned by goroutines.
// It also automatically manages the counter.
func TestErrorGroup(t *testing.T) {
	var wg errgroup.Group

	for i := 0; i < 5; i++ {
		if i == 3 {
			// imitate a function returning non-nil error
			wg.Go(func() error {
				time.Sleep(10 * time.Millisecond)
				return fmt.Errorf("some error")
			})
		} else {
			// a successful goroutine in the group
			id := i

			wg.Go(func() error {
				// no need to decrement counter as with sync.WaitGroup
				fmt.Printf("goroutine %d is working\n", id)
				time.Sleep(100 * time.Millisecond)
				fmt.Printf("goroutine %d is done\n", id)
				return nil
			})
		}

	}

	err := wg.Wait()

	Equal(t, "some error", err.Error())
}

// Can use errgroup.Group with a Context.
// It is useful when we need to let other functions know when goroutines in the group are done.
func TestErrorGroupWithContext(t *testing.T) {
	// context will be cancelled when the group is done
	wg, ctx := errgroup.WithContext(context.Background())

	quit := make(chan struct{})

	// this function need to know when all goroutines in the group finish...
	go func() {
		// do some work...
		<-ctx.Done() // wait for all goroutines to finish
		// do some other work when goroutines are done...
		close(quit)
	}()

	// launch goroutines in an error group
	for i := 0; i < 3; i++ {
		wg.Go(func() error {
			time.Sleep(10 * time.Millisecond)
			return nil
		})
	}

	err := wg.Wait() // wait for goroutines to finish
	Equal(t, nil, err)

	<-quit
}
