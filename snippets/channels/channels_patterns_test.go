package channels

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

// Pattern 1: Goroutine pool
// Use unbuffered channel, because we need the delivery guarantee
func TestGoroutinePool(t *testing.T) {
	const PoolSize int = 2
	const NumTasks int = 10

	// unbuffered channel used to pass work to goroutines in the pool
	ch := make(chan string)

	var results []string
	var mutex sync.Mutex
	var wg sync.WaitGroup
	wg.Add(PoolSize)

	for i := 0; i < PoolSize; i++ {
		// create pool goroutines
		go func(goroutineId int) {
			for data := range ch {
				result := fmt.Sprintf("%s processed by goroutine %d", data, goroutineId)
				mutex.Lock()
				{
					results = append(results, result)
				}
				mutex.Unlock()
			}
			wg.Done() // we exit the range-over-channel loop when the channel is closed
		}(i + 1)
	}

	for i := 0; i < NumTasks; i++ {
		ch <- fmt.Sprintf("work %d", i)
	}
	close(ch) // close the channel once all work is assigned

	// wait for all goroutines in the pool to finish their work
	wg.Wait()

	for _, v := range results {
		fmt.Println(v)
	}

	Equal(t, NumTasks, len(results))
}

// Pattern 2: Fan Out
// Avoid using this in long-running services (like a web server),
// because each request can fan out multiple goroutines, which eventually may lead to
// too many goroutines running at the same time.
// However, this pattern works great for CLI apps and occasional cron jobs.
func TestGoroutineFanOut(t *testing.T) {
	const NumTasks int = 10

	// buffered channel for results
	ch := make(chan string, NumTasks)

	// launch all N goroutines to do some useful work
	for i := 0; i < NumTasks; i++ {
		go func(taskId int) {
			// simulate a long-running process
			time.Sleep(time.Duration(rand.Intn(200)))
			ch <- fmt.Sprintf("task %d done", taskId) // latency can occur if two goroutines finish at the same time
		}(i + 1)
	}

	i := NumTasks
	for i > 0 {
		result := <-ch

		fmt.Println(result)
		i--
	}
}
