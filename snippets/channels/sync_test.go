package channels

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestWaitGroup(t *testing.T) {
	var wg sync.WaitGroup

	for i := 0; i < 3; i++ {
		wg.Add(1)

		go func(wg *sync.WaitGroup, id int) {
			fmt.Printf("goroutine %d is working...\n", id)
			time.Sleep(100 * time.Millisecond)

			wg.Done()
		}(&wg, i)
	}

	wg.Wait() // wait for all goroutines to finish
}

func TestMutex(t *testing.T) {
	m := make(map[int]string)

	var mu sync.RWMutex

	// two goroutines access the same map, one for reading, the other for writing
	// this would be a data race without a mutex
	go func() {
		for i := 0; i < 10; i++ {
			mu.Lock() // lock for writing
			m[i] = fmt.Sprintf("value %d", i)
			mu.Unlock()

			time.Sleep(20 * time.Millisecond)
		}
	}()

	go func() {
		for i := 0; i < 10; i++ {
			mu.RLock() // lock for read access only (more effective)
			fmt.Println(m[i])
			mu.RUnlock()

			time.Sleep(30 * time.Millisecond)
		}
	}()
}

func TestRunOnce(t *testing.T) {
	// some operations such as closing a channel should be performed only once
	// trying to close a channel twice leads to a panic
	ch := make(chan struct{})

	var once sync.Once

	// let's say we have a function that contains some code that is safe to call only once
	f := func(c chan struct{}) {
		fmt.Println("shutting down...")

		// safeguard
		once.Do(func() {
			close(ch)
		})
	}

	// simply calling it twice without sync.Once would lead to a panic...
	f(ch)
	f(ch)
}
