package channels

import (
	"context"
	"fmt"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

func TestContextWithValue(t *testing.T) {
	// context allows to store type-bound value
	type MyKey int

	var key MyKey = 100
	myValue := "test"

	// background context is the base context for all new context instances
	ctx := context.WithValue(context.Background(), key, myValue)

	value := ctx.Value(key)
	Equal(t, "test", value)

	// cannot get value if the type does not match
	value = ctx.Value(100)
	Equal(t, nil, value)
}

func TestContextHierarchy(t *testing.T) {
	type requestId string

	const (
		requestAId requestId = "requestA"
		requestBId requestId = "requestB"
	)

	ctx := context.WithValue(context.Background(), requestAId, 1) // wraps background context
	ctx = context.WithValue(ctx, requestBId, 2)                   // wraps context ctx

	Equal(t, 1, ctx.Value(requestAId))
	Equal(t, 2, ctx.Value(requestBId))
}

func TestContextWithTimeout(t *testing.T) {
	const duration = 50 * time.Millisecond

	ctx, cancel := context.WithTimeout(context.Background(), duration)
	defer cancel()

	ch := make(chan int)

	go func() {
		time.Sleep(100 * time.Millisecond)
		ch <- 1
	}()

	select {
	case _ = <-ch:
		t.Fail()
	case <-ctx.Done():
		// timeout
		fmt.Println("work is cancelled")
	}

	Equal(t, context.DeadlineExceeded, ctx.Err())
}

func TestContextWithCancel(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // must ensure that cancel() is called in any scenario

	// start some listeners
	for i := 0; i < 3; i++ {
		go func(ctx context.Context, id int) {
			fmt.Printf("Listener %d is waiting...\n", id)

			<-ctx.Done() // block until the context is cancelled

			fmt.Printf("Listener %d is exiting.\n", id)
		}(ctx, i)
	}

	time.Sleep(time.Millisecond * 100)

	// cancel the context, so listeners unblock
	// only this and child contexts are cancelled
	cancel()

	time.Sleep(time.Millisecond * 100) // wait for listeners to exit

	Equal(t, context.Canceled, ctx.Err())
}
