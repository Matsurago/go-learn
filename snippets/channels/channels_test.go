package channels

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

// read-write channel is passed to this function
// writing to unbuffered channel is synchronous (blocking); the value must be read to continue
func foo(c chan string) {
	c <- "hello"
	c <- " world"
}

func TestChannel(t *testing.T) {
	c := make(chan string)

	go foo(c) // must be a goroutine or sending (c <- "hello") will block

	result := <-c
	result += <-c

	Equal(t, "hello world", result)
}

// write-only channel is passed to this function
// use 'func producer(c <-chan int)' for read-only channel
func producer(c chan<- int) {
	for i := 1; i < 5; i++ {
		c <- 11 + rand.Intn(1+i)
	}

	close(c) // breaks infinite range loop in test below
}

func TestChannelClose(t *testing.T) {
	c := make(chan int, 3) // buffered channel, capacity = 3

	go producer(c)

	// read from a channel until it is closed
	for value := range c {
		fmt.Println(value)
	}
}

// select is used to receive messages over multiple channels
func TestSelect(t *testing.T) {
	c1 := make(chan string)
	c2 := make(chan string)

	go func(c chan string) {
		time.Sleep(time.Millisecond * 500)
		c <- "hello"
	}(c1)

	go func(c chan string) {
		time.Sleep(time.Millisecond * 500)
		c <- "user"
	}(c2)

	// select is often put into an infinite loop to process messages from channels
	select {
	case msg := <-c1:
		fmt.Println("Message is received on channel 1")
		Equal(t, "hello", msg)
	case msg := <-c2:
		fmt.Println("Message is received on channel 2")
		Equal(t, "user", msg)
	}
}

func TestSelectWithTimeout(t *testing.T) {
	c := make(chan string)

	go func(c chan string) {
		time.Sleep(time.Second * 5)
		c <- "hello"
	}(c)

	select {
	case _ = <-c:
		t.Fail() // timeout should occur first
	case <-time.After(time.Millisecond * 50):
		// success
	}
}

func TestNonBlockingSelect(t *testing.T) {
	// select with default statement is non-blocking
	c := make(chan int)

	go func() {
		time.Sleep(time.Second * 5)
		c <- 42
	}()

	select {
	case _ = <-c: // no data in channel yet, skip this case
		t.Fail()
	default:
		// success
		Equal(t, 0, len(c))
	}
}

func TestSelectToSend(t *testing.T) {
	// select can also be used for non-blocking send
	c1 := make(chan string)    // unbuffered
	c2 := make(chan string, 3) // buffered

	select {
	case c1 <- "a": // blocking op, so this branch is not selected
		t.Fail()
	case c2 <- "b":
		Equal(t, "b", <-c2)
	}
}

func TestChannelClosedFlag(t *testing.T) {
	c := make(chan struct{}) // channel without data
	close(c)

	// wd = with data; it is a boolean flag that is true if data is received
	// from the c; it is false, if something else happened (v.g. channel was closed)
	_, wd := <-c

	False(t, wd)
}

func TestBufferedChannel(t *testing.T) {
	// buffered channel allows non-blocking read/writes until it is full
	c := make(chan string, 3)

	c <- "a" // non-blocking
	c <- "b"

	Equal(t, 2, len(c)) // number of messages in the buffer

	Equal(t, "a", <-c)
	Equal(t, "b", <-c)
}

func TestSendPeriodically(t *testing.T) {
	ch := time.Tick(100 * time.Millisecond) // send to this channel every 100ms
	quit := make(chan struct{})
	var i int

	go func() {
		for {
			<-ch
			i++

			if i == 3 {
				close(quit)
			}
		}
	}()

	<-quit
}

// use case unknown?
// we normally want to execute after some event, this is too non-deterministic
func TestExecuteAfterDelay(t *testing.T) {
	ch := make(chan struct{})

	f := func() {
		fmt.Println("closing the channel...")
		close(ch)
	}

	time.AfterFunc(200*time.Millisecond, f)
	<-ch

	fmt.Println("exiting.")
}
