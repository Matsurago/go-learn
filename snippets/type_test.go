package main

import (
	"fmt"
	"reflect"
	"slices"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

// can define type aliases
type MyInt int
type Numbers []int

// can define methods on user-defined types
func (ns Numbers) Sum() int {
	res := 0
	for _, val := range ns {
		res += val
	}
	return res
}

// define String method to define string representation used by the 'fmt' package
func (x MyInt) String() string {
	return fmt.Sprintf("(val: %d)", x)
}

func TestTypeAliases(t *testing.T) {
	// need to convert a const into the user-defined type
	x := MyInt(4)
	True(t, x == 4) // alias inherits all base type operations

	xs := Numbers{3, 4, 7}
	Equal(t, 14, xs.Sum())

	Equal(t, "(val: 4)", fmt.Sprintf("%v", x))
}

func TestReflectionGetType(t *testing.T) {
	typ := reflect.TypeOf(true)
	Equal(t, "bool", fmt.Sprint(typ))

	typ = reflect.TypeOf("abcd")
	Equal(t, "string", fmt.Sprint(typ))

	typ = reflect.TypeOf(1.9283827377332)
	Equal(t, "float64", fmt.Sprint(typ))

	typ = reflect.TypeOf(-12092339)
	Equal(t, "int", fmt.Sprint(typ))

	type Temp struct{ Value int }
	var temp Temp
	typ = reflect.TypeOf(temp)
	Equal(t, "main.Temp", fmt.Sprint(typ))
}

func TestReflectionEnumerateMethods(t *testing.T) {
	listMethods := func(x interface{}) []string {
		v := reflect.ValueOf(x)
		typ := v.Type()

		var res []string

		for i := 0; i < v.NumMethod(); i++ {
			res = append(res, typ.Method(i).Name)
		}
		return res
	}

	res := listMethods(time.Hour)
	True(t, slices.Contains(res, "Seconds"))
	True(t, slices.Contains(res, "Hours"))
	True(t, slices.Contains(res, "String"))
}
