package main

import (
	"errors"
	"fmt"
	"os"
	"testing"

	. "github.com/stretchr/testify/assert"
)

// basic error
func TestCreateError(t *testing.T) {
	code := 42

	err := fmt.Errorf("error, code %d", code)

	Equal(t, "error, code 42", err.Error())
}

// custom error type with extra info
type MyError struct {
	Code    int
	Message string
}

func (e MyError) Error() string {
	return fmt.Sprintf("error %d: %s", e.Code, e.Message)
}

func TestCustomError(t *testing.T) {
	foo := func(flag bool) error { // always return the interface error type, not a custom one
		var err error
		if flag {
			err = MyError{
				Code:    1,
				Message: "some error",
			}
		}
		return err
	}

	err := foo(false)
	Equal(t, nil, err)

	err = foo(true)
	Equal(t, "error 1: some error", err.Error())
}

// wrapped errors
func TestWrappedError(t *testing.T) {
	foo := func() error {
		_, err := os.Open("not_found.txt")
		if err != nil {
			// wrap underlying error
			return fmt.Errorf("error in foo: %w", err) // use %w to wrap; %v means 'no wrap, just insert base error message'
		}
		return nil
	}

	err := foo()
	Equal(t, "error in foo: open not_found.txt: The system cannot find the file specified.", err.Error())

	// check if there is a specific wrapped error inside
	True(t, errors.Is(err, os.ErrNotExist))

	// unwrap error
	baseErr := errors.Unwrap(err)
	NotEqual(t, nil, baseErr) // nil check is a must
	Equal(t, "open not_found.txt: The system cannot find the file specified.", baseErr.Error())
}
