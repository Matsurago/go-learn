package main

import (
	"math"
	"strconv"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestSqrt(t *testing.T) {
	res := math.Sqrt(4)

	Equal(t, "2.00", strconv.FormatFloat(res, 'f', 2, 64))
}

func TestRounding(t *testing.T) {
	f := 6.5
	Equal(t, float64(7), math.Round(f))
	Equal(t, float64(6), math.RoundToEven(f))
	Equal(t, float64(7), math.Ceil(f))
	Equal(t, float64(6), math.Floor(f))
}
