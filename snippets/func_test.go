package main

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

// can use concise argument declaration if arguments are of the same type
// but don't do this: error-prone
func pow(x, n int) int {
	res := x
	for i := 2; i <= n; i++ {
		res *= x
	}
	return res
}

func TestConciseArgs(t *testing.T) {
	Equal(t, 64, pow(2, 6))
}

// can have variadic number of arguments
func mySum(args ...int) int {
	s := 0
	for _, val := range args {
		s += val
	}
	return s
}

func TestVarargs(t *testing.T) {
	Equal(t, 10, mySum(2, 3, 5))
	Equal(t, 0, mySum())

	// to pass a slice, expand it
	nums := []int{9, 11, 17}
	Equal(t, 37, mySum(nums...))
}

// function can return multiple values
func TestFunctionMultipleReturnValues(t *testing.T) {
	f := func() (int, string) {
		return 4, "test"
	}

	x, s := f()
	Equal(t, 4, x)
	Equal(t, s, "test")
}

// function can have named return values
// but don't use it: error-prone
func addSubtract(a, b int) (addition int, difference int) {
	addition = a + b
	difference = a - b
	return // bare return when using named return values
}

func TestFunctionNamedReturnValues(t *testing.T) {
	s, d := addSubtract(5, 7)
	Equal(t, 12, s)
	Equal(t, -2, d)
}

// if a function returns multiple values, it can be used as an input for
// another function which expects the same values
func TestPassFunction(t *testing.T) {
	f := func() (int, int) {
		return 2, 4
	}

	g := func(x int, y int) int {
		return x + y
	}

	result := g(f()) // can call it like this (only in Go!)

	Equal(t, 6, result)
}

// Go has lambdas
func sort(a []int, cmp func(int, int) bool) {
	for i := 1; i < len(a); i++ {
		pos := i
		cur := a[i]

		for pos > 0 && cmp(cur, a[pos-1]) {
			a[pos] = a[pos-1]
			pos--
		}

		a[pos] = cur
	}
}

func TestLambda(t *testing.T) {
	input := []int{8, 3, 1, 2, 5, 0, 9, 4}
	expected := []int{0, 1, 2, 3, 4, 5, 8, 9}

	sort(input, func(x, y int) bool {
		return x < y
	})

	Equal(t, expected, input)

	input = []int{8, 3, 1, 2, 5, 0, 9, 4}
	expected = []int{9, 8, 5, 4, 3, 2, 1, 0}

	sort(input, func(x, y int) bool {
		return x > y
	})

	Equal(t, expected, input)
}

func TestFunctionTypeAlias(t *testing.T) {
	type BinOp func(int, int) int // can make function alias

	f := func(op BinOp, x int) int {
		return op(x, x)
	}

	Equal(t, 256, f(pow, 4))
}

// Go also has closures
func inc(x int) func() int {
	return func() int {
		x++
		return x
	}
}

func TestClosure(t *testing.T) {
	adder := inc(70)

	Equal(t, 71, adder())
	Equal(t, 72, adder())
}

func TestClosureValueIsReferenced(t *testing.T) {
	name := "Name"
	f := func() string {
		return name + "!"
	}

	name = "Vasya" // this also affects closure f

	// to avoid this side effect, copy all values passed to closure
	Equal(t, "Vasya!", f())
}
