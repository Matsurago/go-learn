package structs

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

// any type based on a reference type should use value method semantics
type MyInts []int

// using value semantics (notice that method returns a copy of object of the same type)
func (a MyInts) update(pos int, value int) MyInts {
	result := make(MyInts, len(a))
	for i := range a {
		result[i] = a[i]
	}
	result[pos] = value
	return result
}

func TestMethodValueSemantics(t *testing.T) {
	arr := MyInts{1, 2, 3}

	Equal(t, 1, arr[0])

	result := arr.update(0, 9)

	Equal(t, 1, arr[0])
	Equal(t, 9, result[0])
}

// using pointer semantics (in practice, never mix different semantics for the same type!)
func (a *MyInts) set(pos int, value int) {
	(*a)[pos] = value
}

func TestMethodPointerSemantics(t *testing.T) {
	arr := MyInts{1, 2, 3}

	Equal(t, 1, arr[0])

	arr.set(0, 9)

	Equal(t, 9, arr[0])
}

// user-defined structs mostly use pointer semantics,
// except for obviously "value-like" types like Time struct
type CustomDevice struct {
	Name     string
	Location string
}

func (d *CustomDevice) moveToStorage() {
	d.Location = "storage"
}

func TestMethodPointerSemanticsForUserDefinedStruct(t *testing.T) {
	device := CustomDevice{"Coffee Machine", "kitchen"}
	device.moveToStorage()

	Equal(t, "storage", device.Location)

	devices := []CustomDevice{
		{"PC", "office area"},
		{"Printer", "office area"},
	}

	// we must also use pointer semantics in range loops
	for _, d := range devices {
		d.moveToStorage()
	}

	// NOTE: the location is not changed
	Equal(t, "office area", devices[0].Location)
	Equal(t, "office area", devices[1].Location)
}

// An example of user-defined type that uses value semantics
type CustomPoint struct {
	x int
	y int
}

func (p CustomPoint) shift(deltaX, deltaY int) CustomPoint {
	// p is a copy of the original point
	p.x += deltaX
	p.y += deltaY
	return p
}

func TestMethodValueSemanticsForUserDefinedStruct(t *testing.T) {
	p1 := CustomPoint{0, 0}
	Equal(t, 0, p1.x)

	p2 := p1.shift(2, 2)

	Equal(t, 0, p1.x)
	Equal(t, 2, p2.x)
}

// methods are really just a syntactic sugar
func TestMethodIsSyntacticSugar(t *testing.T) {
	p1 := CustomPoint{}

	p2 := CustomPoint.shift(p1, 2, 2)

	Equal(t, 2, p2.x)
}

// function variable is a 2-word data structure [x | x]:
// {pointer to code, pointer to a copy of the object of the type (when using value semantics)}
func TestFunctionVariableToMethod(t *testing.T) {
	p1 := CustomPoint{}

	// f is a 2-word data structure [x | x]
	// the 1st word points to the shift function
	// the 2nd word points to a newly allocated copy of p1
	f := p1.shift

	// we can now call the method using our new function variable
	f(2, 2)

	// p1 is not changed
	Equal(t, 0, p1.x)
}
