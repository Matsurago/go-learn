package structs

import (
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

type Pizza struct {
	Name string
	Size int
}

// a const method on struct
func (p Pizza) half() Pizza {
	return Pizza{
		Name: p.Name,
		Size: p.Size / 2,
	}
}

// a method on struct that can mutate data
func (p *Pizza) halved() {
	p.Size = p.Size / 2
}

func TestEmptyStruct(t *testing.T) {
	var p Pizza

	Equal(t, "{Name: Size:0}", fmt.Sprintf("%+v", p))
}

func TestStructLiteral(t *testing.T) {
	p := Pizza{
		Name: "Margherita",
		Size: 15,
	}

	Equal(t, "{Name:Margherita Size:15}", fmt.Sprintf("%+v", p))
}

func TestShortStructLiteral(t *testing.T) {
	type Point struct {
		x int
		y int
	}

	p := Point{3, 4}

	Equal(t, "{x:3 y:4}", fmt.Sprintf("%+v", p))
}

func TestStructMethods(t *testing.T) {
	p := Pizza{"Test", 18}
	p2 := p.half()
	Equal(t, p2.Size, 9)

	Equal(t, 18, p.Size)
	p.halved()
	Equal(t, 9, p.Size)
}

func TestStructComparison(t *testing.T) {
	p1 := Pizza{"Margherita", 15}
	p2 := Pizza{"Margherita", 15}

	Equal(t, p1, p2)

	p2.Size = 17
	NotEqual(t, p1, p2)
}

func TestStructCopy(t *testing.T) {
	p1 := Pizza{"Margherita", 15}

	// assignment is copy for structs
	p2 := p1

	p2.Size = 19

	Equal(t, 19, p2.Size)
	Equal(t, 15, p1.Size)
}

func TestAnonymousStruct(t *testing.T) {
	s := struct {
		counter bool // note: takes 8 bytes instead of 5 because of padding
		value   int
	}{
		counter: true,
		value:   42,
	}

	Equal(t, 42, s.value)
}

// can implement String() method of Stringer interface
type MyPoint struct {
	x int
	y int
}

func (p MyPoint) String() string {
	return fmt.Sprintf("(%d,%d)", p.x, p.y)
}

func TestToString(t *testing.T) {
	p := MyPoint{7, 9}

	Equal(t, "(7,9)", fmt.Sprint(p))
}

// implicit conversion is prohibited
func TestExplicitConversion(t *testing.T) {
	type TypeA struct {
		value   string
		enabled bool
	}

	type TypeB struct {
		value   string
		enabled bool
	}

	var a TypeA
	var b TypeB
	b.enabled = true

	// a = b  // does not compile
	a = TypeA(b) // using conversion

	Equal(t, true, a.enabled)
}
