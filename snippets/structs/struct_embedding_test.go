package structs

import (
	"bytes"
	"io"
	"testing"

	. "github.com/stretchr/testify/assert"
)

// Structs can be embedded to model "is-a" relationship
type Person struct {
	LastName string
}

type Employee struct {
	Person     // embedding
	HourlyRate int
}

func TestEmbeddedStruct(t *testing.T) {
	e := Employee{
		HourlyRate: 15,
		Person: Person{
			LastName: "Ivanov",
		},
	}

	// can access Person's fields directly in Employee (if it is unambiguous)
	Equal(t, "Ivanov", e.LastName)

	// can always access by the full path
	Equal(t, "Ivanov", e.Person.LastName)
}

// Employee also gets this method because of embedding
func (p *Person) initial() string {
	return p.LastName[:1]
}

func TestEmbeddedStructMethodPromotion(t *testing.T) {
	e := Employee{
		HourlyRate: 15,
		Person: Person{
			LastName: "Ivanov",
		},
	}

	Equal(t, "I", e.initial())
}

// when embedding a struct with a pointer, the pointer will be nil
type User struct {
	Name string
}

type Admin struct {
	*User
	AccessLevel int
}

func TestEmbeddedPointer(t *testing.T) {
	a := Admin{} // pointer to User is nil, may cause panic
	True(t, a.User == nil)

	a = Admin{
		User:        &User{},
		AccessLevel: 1,
	}
	True(t, a.User != nil)
}

// can also embed interfaces; that makes the struct to conform to the interface
// but the actual instance that implements the interface should be assigned;
// failure to do so leads to panic
type IO struct {
	io.Writer // Writer is an interface
}

func TestEmbeddedInterface(t *testing.T) {
	i := IO{
		Writer: &bytes.Buffer{}, // must specify which object implements the interface
	}

	n, err := i.Write([]byte("test")) // panics if Writer is not set

	Equal(t, 4, n)
	Equal(t, nil, err)
}
