package structs

import (
	"sort"
	"testing"

	. "github.com/stretchr/testify/assert"
)

type ticket struct {
	venue string
	no    int
}

// need to create an alias for a slice of the struct type
// and make that alias type to implement the sort.Interface interface

type tickets []ticket

func (ts tickets) Len() int {
	return len(ts)
}

func (ts tickets) Less(i, j int) bool {
	return ts[i].no < ts[j].no
}

func (ts tickets) Swap(i, j int) {
	ts[i], ts[j] = ts[j], ts[i]
}

func TestSortStructs(t *testing.T) {
	ts := tickets{
		{"ACME Avenue", 7},
		{"ACME Avenue", 3},
		{"Future City", 5},
		{"Future City", 2},
	}

	False(t, sort.IsSorted(ts))

	sort.Sort(ts)
	True(t, sort.IsSorted(ts))
	Equal(t, 2, ts[0].no)
	Equal(t, 7, ts[3].no)
}
