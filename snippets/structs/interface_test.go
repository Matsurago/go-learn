package structs

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"math"
	"strconv"
	"testing"

	. "github.com/stretchr/testify/assert"
)

type Figure interface {
	Area() float64
}

type Circle struct {
	Radius float64
}

// the receiver for a particular data type should always be
// either a value or a pointer; do not mix them for the same data type
func (c Circle) Circumference() float64 {
	return 2 * math.Pi * c.Radius
}

// implement the interface Figure for type Circle
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// assert that a type implements an interface
var _ Figure = Circle{} // usually done at package level

// every type implements the empty interface{} (any is a synonym for interface{})
var _ any = Circle{}
var _ interface{} = Circle{} // pre Go 1.18

// note that we use value semantics for interfaces
// interface value is a 2-word data structure [x | x]:
// the 1st word is a pointer to iTable (similar to vtable in C++),
// the 2nd word is a pointer to a _copy_ of the real object behind the interface
func tenTimesArea(f Figure) float64 {
	return 10 * f.Area()
}

func TestMethods(t *testing.T) {
	c1 := Circle{1.0}
	result := c1.Circumference()

	Equal(t, "6.28", strconv.FormatFloat(result, 'f', 2, 64))

	c2 := Circle{2.0}
	result = tenTimesArea(c2)

	Equal(t, "125.66", strconv.FormatFloat(result, 'f', 2, 64))
}

func TestInterfaceStructure(t *testing.T) {
	// as we said, interface is a 2-word data structure that makes a copy
	// when value semantics is used
	c1 := Circle{1.0}

	var fig Figure = c1 // fig: [iTable | copy of c1]

	c1.Radius = 2.0

	// modification of c1 is not visible, as the interface data structure got its own copy
	Equal(t, "3.14", strconv.FormatFloat(fig.Area(), 'f', 2, 64))
}

// Now, suppose we have a struct that uses pointer semantics...
type Building struct {
	Name        string
	MainArea    float64
	UtilityArea float64
}

func (b *Building) Area() float64 {
	return b.MainArea + b.UtilityArea
}

func TestInterfacePointerSemantics(t *testing.T) {
	b := Building{"factory", 20.0, 10.0}

	// we cannot call 'tenTimesArea(b)', because we are not allowed to make copies
	// of data that uses pointer semantics! We can share the data though.
	result := tenTimesArea(&b)

	Equal(t, "300.00", strconv.FormatFloat(result, 'f', 2, 64))
}

// interfaces can be tested to determine type
func TestInterfaceDataType(t *testing.T) {
	var f Figure
	f = Circle{1.0}

	_, ok := f.(Circle)

	True(t, ok)
}

// switch can be used to do instance-of check
func TestInterfaceDataTypeSwitch(t *testing.T) {
	var f Figure
	f = Circle{1.0}

	var isCircle bool

	switch f.(type) {
	case Circle:
		isCircle = true
	default:
		isCircle = false
	}

	True(t, isCircle)
}

// interface comparison really compares values (or pointers) inside them
func TestInterfaceValueComparison(t *testing.T) {
	var f, g Figure
	f = Circle{1.0}
	g = Circle{1.0}

	// circle is using value semantics
	True(t, f == g)
}

func TestInterfacePointerComparison(t *testing.T) {
	// if a type uses pointer semantics, interface stores a pointer,
	// so no two pieces of data are the same
	var e1, e2 error
	e1 = errors.New("test")
	e2 = errors.New("test")

	False(t, e1 == e2)
}

// PITFALL
// comparing interface values of incomparable types results in !runtime! panic
func TestInterfaceIncomparablePanics(t *testing.T) {
	var x interface{} = make([]byte, 8)
	var y interface{} = make([]byte, 8)

	Panics(t, func() { fmt.Print(x == y) })
}

// PITFALL
// interface is not nil even when underlying value is nil
func TestInterfaceIsNotNil(t *testing.T) {
	var buf *bytes.Buffer
	var w io.Writer = buf

	True(t, buf == nil)
	False(t, w == nil)
}

// to create a composite interface, embed it into another
type Sender interface {
	Send() error
}

type Receiver interface {
	Receive() error
}

type Communicator interface {
	Sender
	Receiver

	Repeat() // can also declare additional methods
}

// PITFALL
// in other languages an object's type (T) fully determines if the object conforms to an interface;
// in Go, it depends whether it's a value (T) or a pointer (*T).
// an object conforms to an interface if its method set includes all methods from the interface, but:
// for a pointer: both methods with pointer and value semantics count to this object's method set
// for a value: only methods with value semantics count to this object's method set
type Foo struct {
	a int
}

func (f *Foo) String() string { // pointer semantics
	return fmt.Sprintf("(val: %d)", f.a)
}

func TestStructMethodSet(t *testing.T) {
	f := Foo{}
	var s1 fmt.Stringer = &f // compiles
	//var s2 fmt.Stringer = f  // doesn't compile: f's method set doesn't include String()

	Equal(t, "(val: 0)", s1.String())
}
