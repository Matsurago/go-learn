package web

import (
	"bytes"
	"html/template"
	"os"
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

type ticket struct {
	Id    int
	Date  time.Time
	Venue string
}

func TestRenderBasicTemplate(t *testing.T) {
	tmpl, err := template.ParseFiles("templates/simple.html")
	Equal(t, nil, err)

	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, "sample text")
	Equal(t, nil, err)

	Equal(t, "<h1>Value: sample text</h1>\r\n", buf.String())
}

func TestRenderTemplate(t *testing.T) {
	tmpl, err := template.ParseFiles("templates/ticket.html")
	Equal(t, nil, err)

	tickets := []ticket{
		{
			Id:    10001,
			Date:  time.Date(2022, 1, 1, 12, 15, 0, 0, time.UTC),
			Venue: "Oxford Theater",
		},
		{
			Id:    10002,
			Date:  time.Date(2022, 1, 5, 9, 10, 0, 0, time.UTC),
			Venue: "Oxford Theater",
		},
	}

	buf := new(bytes.Buffer)
	err = tmpl.ExecuteTemplate(buf, "ticket.html", tickets)
	Equal(t, nil, err)

	res := buf.String()

	exp, err := os.ReadFile("testdata/expected_ticket.html")
	Equal(t, nil, err)

	Equal(t, string(exp), res)
}
