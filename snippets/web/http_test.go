package web

import (
	"io"
	"net/http"
	"strings"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestHttpGet(t *testing.T) {
	response, err := http.Get("https://google.com")
	defer response.Body.Close()

	Equal(t, nil, err)
	Equal(t, 200, response.StatusCode)

	bytes, err := io.ReadAll(response.Body)
	Equal(t, nil, err)

	content := string(bytes)
	True(t, strings.Contains(content, "Google"))
}
