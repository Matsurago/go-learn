package main

import (
	"fmt"
	"math"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestVarZeroInit(t *testing.T) {
	var val int
	Equal(t, 0, val)

	var x float64
	Equal(t, 0.0, x)

	var s string // string is a 2-word data structure: {pointer to data, size}
	Equal(t, "", s)

	var b bool
	Equal(t, false, b)

	var i fmt.Stringer // zero value for an interface is nil
	Equal(t, nil, i)

	var ptr *int // zero value for a pointer is nil
	True(t, ptr == nil)

	var f func(int, int) bool // zero value for a function literal is nil
	True(t, f == nil)
}

func TestVarInit(t *testing.T) {
	// short init (only local variables)
	otherVal := 12
	Equal(t, 12, otherVal)

	// init block
	var (
		x = 3
		y = 5
	)
	Equal(t, 3, x)
	Equal(t, 5, y)
}

func TestVarSwap(t *testing.T) {
	// can declare and swap values without a tmp var
	a, b := 3, 4
	a, b = b, a

	Equal(t, 4, a)
	Equal(t, 3, b)
}

func TestIota(t *testing.T) {
	const (
		a = iota + 1
		b
		c
	)

	Equal(t, 1, a)
	Equal(t, 2, b)
	Equal(t, 3, c)
}

func TestShiftedIota(t *testing.T) {
	const (
		a = 1 << iota
		b
		c
		d
	)

	Equal(t, 1, a)
	Equal(t, 2, b)
	Equal(t, 4, c)
	Equal(t, 8, d)
}

func TestNaN(t *testing.T) {
	zero := 0.0

	True(t, math.IsNaN(0/zero))

	True(t, math.IsInf(100/zero, 1))
	True(t, math.IsInf(-100/zero, -1))
}

func TestUInt(t *testing.T) {
	var u uint8

	// numeric types underflow and overflow at runtime
	Equal(t, uint8(0), u)
	Equal(t, uint8(255), u-1) // underflow

	var k uint8 = 26
	Equal(t, uint8(4), u+k*10) // overflow
}
