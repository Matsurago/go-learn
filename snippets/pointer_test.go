package main

import (
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestPointers(t *testing.T) {
	a := 1   // int
	pa := &a // a pointer to int

	foo(t, a, pa)
}

// this function receives a copy of 'a' and a pointer to the real 'a' in main()
func foo(t *testing.T, a int, prev *int) {
	fmt.Printf("addr(a) = %v, addr(a in main) = %v\n", &a, prev)

	True(t, &a != prev)
	Equal(t, 1, *prev) // following a pointer (dereferencing)
}

func TestPointersToPrimitiveTypes(t *testing.T) {
	ps := new(string)
	Equal(t, "", *ps)

	*ps = "test"
	Equal(t, "test", *ps)
}

func TestPointerToPointer(t *testing.T) {
	a := 1     // int
	pa := &a   // *int
	ppa := &pa // **int

	**ppa = 3

	Equal(t, 3, **ppa)
	Equal(t, 3, a)
}

/// Escape Analysis

// A value may escape to the heap in 3 cases:
//   1) when sharing a value up the call stack (see the example below);
//   2) when a value is shared between multiple stack frames (?)
//   3) when the compiler does not know in advance the size of the data

// let's have a simple struct
type entry struct {
	username string
	id       int
}

// this function returns a pointer...
func pointerMaker() *entry {
	e := entry{ // this value is allocated on the stack frame
		username: "Karl",
		id:       400,
	}

	return &e // the value must escape this stack frame (or it will be erased)
}

func TestEscapeAnalysis(t *testing.T) {
	pe := pointerMaker() // the value is escaped to the heap
	Equal(t, "Karl", pe.username)
}
