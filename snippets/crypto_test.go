package main

import (
	"crypto/sha256"
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestShaDigest(t *testing.T) {
	digest1 := sha256.Sum256([]byte("hello world")) // type is []byte
	digest2 := sha256.Sum256([]byte("hello world"))

	True(t, digest1 == digest2)

	digest3 := sha256.Sum256([]byte("hello 世界"))
	False(t, digest1 == digest3)

	Equal(t, "2e2625f7c51b4a2c75274ab307e86411f57aab475f4a4078df53533f7771bc7f",
		fmt.Sprintf("%x", digest3))
}
