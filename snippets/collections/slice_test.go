package collections

import (
	"fmt"
	"slices"
	"strings"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestSliceLiteral(t *testing.T) {
	a := []string{"cat", "mouse"}

	Equal(t, "[cat mouse]", fmt.Sprint(a))
}

func TestEmptySliceLiteral(t *testing.T) {
	var a []int // can also use 'a := []int{}'
	Equal(t, "[]", fmt.Sprint(a))

	a = append(a, 3) // can append to nil slice
	Equal(t, "[3]", fmt.Sprint(a))
}

func TestEmptySliceWithCapacity(t *testing.T) {
	a := make([]string, 0, 2)

	Equal(t, 0, len(a))
	Equal(t, 2, cap(a))
}

func TestSliceDoublingCapacityOnResize(t *testing.T) {
	a := []string{"a", "b", "c", "d"}
	Equal(t, 4, cap(a))

	a = append(a, "e", "f")
	Equal(t, 8, cap(a))

	// after some point the size is not doubled, but increased by at least 25%
	b := make([]string, 1024)
	b = append(b, "test")

	True(t, cap(b) >= int(1024*1.25))
}

func TestAppendToSlice(t *testing.T) {
	var a []string
	a = append(a, "test")           // add a single value
	a = append(a, "xx", "yy", "zz") // add multiple values

	b := []string{"cat", "mouse"}
	a = append(a, b...) // add another slice

	Equal(t, "[test xx yy zz cat mouse]", fmt.Sprint(a))
}

func TestDeleteFromSlice(t *testing.T) {
	a := []string{"a", "b", "c", "d", "e"}

	a = append(a[:2], a[3:]...) // spread operator

	Equal(t, 4, len(a))
	Equal(t, "[a b d e]", fmt.Sprint(a))
}

func TestSliceToString(t *testing.T) {
	a := []string{"a", "b", "c"}

	Equal(t, "a:b:c", strings.Join(a, ":"))
}

func TestSliceComparison(t *testing.T) {
	a := []string{"a", "b"}
	b := []string{"a", "b"}

	True(t, slices.Equal(a, b))
}

func TestSliceClear(t *testing.T) {
	// clearing a slice doesn't change its length or capacity
	// it sets all elements to their zero value
	a := []string{"one", "two", "three"}

	clear(a)

	Equal(t, 3, len(a))
	Equal(t, "", a[0])
	Equal(t, "", a[2])
}

// PITFALL
func TestSliceView(t *testing.T) {
	a := []int{1, 2, 3, 4, 5}

	// slice view: [start, end); the backing array is shared, capacity stays
	b := a[1:2]
	Equal(t, 1, len(b))
	Equal(t, 4, cap(b))
	Equal(t, 2, b[0])

	// changing this view modifies the original slice!
	b[0] = 99
	Equal(t, "[99]", fmt.Sprint(b))
	Equal(t, "[1 99 3 4 5]", fmt.Sprint(a))

	// appending to b also modifies a!
	b = append(b, 100)
	Equal(t, "[99 100]", fmt.Sprint(b))
	Equal(t, "[1 99 100 4 5]", fmt.Sprint(a)) // the remaining elements of 'a' are not altered

	// one way to avoid such side effects with append (but not modification)
	// is to limit the capacity of the slice; this will result on copy-on-write
	c := []int{1, 2, 3}
	d := c[1:2:2] // capacity is from the beginning of the original slice

	Equal(t, 1, cap(d))

	d = append(d, 9)

	Equal(t, "[2 9]", fmt.Sprint(d))
	Equal(t, "[1 2 3]", fmt.Sprint(c))
}

// PITFALL
func TestReferenceToSliceElement(t *testing.T) {
	type counter struct {
		value int
	}

	counters := make([]counter, 3, 3)
	counter1 := &counters[1] // share a second counter to work with it

	counter1.value++
	Equal(t, 1, counters[1].value) // increment is seen is the slice

	// however, if we append to a slice, our backing array may be re-allocated..
	counters = append(counters, counter{})

	counter1.value++

	// further increment is no longer visible
	// our pointer counter1 points to the OLD backing array
	Equal(t, 1, counters[1].value)
}

// PITFALL
func TestSlicesForRange(t *testing.T) {
	a := []int{1, 2, 3, 4, 5}

	// for used with value semantics operates on a copy of the slice
	sum := 0
	for _, v := range a {
		a = a[:2] // this modifies the original slice, the copy is unchanged, so the cycle runs 5 times
		sum += v
	}

	Equal(t, 15, sum)

	// for used with pointer semantics operates on the original slice
	sum = 0
	for i := range a {
		a = a[:2] // now this cycle is going to run only 2 times
		sum += a[i]
	}

	Equal(t, 3, sum)
}

func TestSliceCopy(t *testing.T) {
	src := []int{1, 2, 3}
	dst := make([]int, 5)
	dst[3] = 100

	copy(dst, src)

	// assignment to src has now no effect to dst
	src[0] = 4
	Equal(t, "[4 2 3]", fmt.Sprint(src))
	Equal(t, "[1 2 3 100 0]", fmt.Sprint(dst)) // elements outside of copy range are preserved in dst

	// if dst has fewer elements, only that number of elements is copied
	dst = make([]int, 2)
	copy(dst, src)

	Equal(t, 2, len(dst))
	Equal(t, "[4 2]", fmt.Sprint(dst))
}
