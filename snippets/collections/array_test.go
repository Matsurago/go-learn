package collections

import (
	"fmt"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestArrayInitTest(t *testing.T) {
	var a [5]string

	Equal(t, "", a[4])
}

func TestArrayLiterals(t *testing.T) {
	// the remaining elements are zeroed
	a := [3]int{1, 2}

	Equal(t, 0, a[2])

	// the number of elements is deduced
	b := [...]int{1, 2, 3, 4, 5}

	Equal(t, 5, len(b))

	// can specify elements at particular indexes
	c := [...]int{50: -1, 99: -1}

	Equal(t, 100, len(c))
	Equal(t, 0, c[0])
	Equal(t, -1, c[99])
}

func TestArrayComparison(t *testing.T) {
	// unlike slices, arrays can be compared
	a := [3]int{1, 2}
	b := [...]int{1, 2, 0}

	True(t, a == b)
}

func TestArrayCopy(t *testing.T) {
	a := [3]int{1, 2, 3}
	b := [3]int{4, 5, 6}

	a = b // this copies the underlying memory for arrays!
	a[0] = 7

	Equal(t, "[7 5 6]", fmt.Sprint(a))
	Equal(t, "[4 5 6]", fmt.Sprint(b)) // b is unchanged
}

func TestRangeIteration(t *testing.T) {
	a := [3]int{10, 20, 70}
	var sum, isum int

	// index, value
	for i, val := range a {
		sum += val
		isum += i
	}

	Equal(t, 100, sum)
	Equal(t, 3, isum)
}

func TestArrayLoopUsingValueSemantics(t *testing.T) {
	a := [3]string{"axis", "beaver", "cinammon"}

	// using value semantics; val gets a copy, the original array is unchanged
	for _, val := range a {
		val = "something else"
		Equal(t, "something else", val)
	}

	Equal(t, "axis", a[0])
	Equal(t, "beaver", a[1])
	Equal(t, "cinammon", a[2])
}
