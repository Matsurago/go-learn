package collections

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestEmptyMap(t *testing.T) {
	m := map[string]int{}

	// if a value is not in map, zero value of the appropriate type is returned
	Equal(t, 0, m["test"])
}

func TestEmptyMapWithTypeAlias(t *testing.T) {
	type StrIntMap map[string]int

	m := StrIntMap{}
	Equal(t, 0, m["test"])
}

func TestMapGetSet(t *testing.T) {
	m := map[string]int{}

	m["one"] = 1
	m["two"] = 2

	Equal(t, 2, m["two"])

	delete(m, "one")

	value, found := m["one"]
	Equal(t, 0, value)
	False(t, found)
}

func TestMapContainsKey(t *testing.T) {
	m := map[string]int{}
	m["ten"] = 10

	_, ok := m["ten"]
	True(t, ok)

	_, ok = m["two"]
	False(t, ok)
}

func TestMapContainsKeyWithIfExpr(t *testing.T) {
	m := map[string]int{
		"one": 1,
		"two": 2,
	}

	// can limit the scope of temp variables
	if val, ok := m["one"]; ok {
		Equal(t, 1, val)
	}
}

func TestMapIteration(t *testing.T) {
	m := map[string]int{
		"one": 1,
		"two": 2,
	}

	// the order of key/value pairs is randomized
	for key, value := range m {
		Equal(t, value, m[key])
	}
}

func TestMapWithStruct(t *testing.T) {
	type Person struct {
		Name string
		Age  int
	}

	m := map[int]Person{
		1: {Name: "Serj", Age: 30},
		2: {Name: "Olga", Age: 27},
	}

	// cannot update struct value in map in place:
	// m["Olga"].Age = 28  // doesn't work!

	// need to retrieve the value first
	p := m[1]
	p.Age++
	m[1] = p // and then put it back after the update

	Equal(t, 31, m[1].Age)
}

func TestMapClear(t *testing.T) {
	m := map[int]string{
		1: "one",
		2: "two",
		3: "three",
	}

	Equal(t, 3, len(m))

	// clearing a map erases all key/value pairs
	// this is very different from clearing a slice
	clear(m)

	Equal(t, 0, len(m))
}
