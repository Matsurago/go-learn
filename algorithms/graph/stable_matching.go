/* Stable Matching

Given a set of men M and a set of women W (or a set of applicants and employers),
both sets of size n, such that men have preferences (ranking) of women, and women
also have ranking of men (each man or women assigns unique rank to each person of
the opposite sex), find a set S = {(m, w), m ∈ M, w ∈ W} such that:

  1) it is a matching, i.e. every man or woman may appear only once:
     for any pairs (m, w) and (m', w') ∈ S => m != m', w != w'
  2) it is a perfect matching, i.e. set S contains all man and women and has size n
  3) it is a stable matching, i.e. there is no such a man and a woman that would prefer
	 each other rather than their current partners; in other words,
	 there are no two pairs (m, w) and (m', w') ∈ S such that
	 m ranks w' over w, and w' ranks m over m' at the same time.
	 (if there were, m and w' could abandon their current partners and make a couple.)
*/

package graph

import (
	"fmt"
	"gitlab.com/Matsurago/go-learn/algorithms/ds"

	"gitlab.com/Matsurago/go-learn/stdext"
)

/*
StableMatching produces a stable perfect matching of men and women given the preference
matrices for both genders.

Preconditions:
 1. menPrefs, womenPrefs are both square matrices of size n
 2. i-th column is a ranking of persons of the opposite gender for person i, for example:
    col-1: [4 2 1 3] means that man 1 prefers woman 3 over all; the next best candidate
    is woman 2; the next is woman 4; and woman 1 is the least favorable (she's on 4th place);
    note that all indexes are zero-based
 3. no matix column has duplicate values

Postconditions:
  - result contains stable perfect matching (value v at i-th index denotes a pair (i, v))
*/
func StableMatching(menPrefs, womenPrefs [][]int) []int {
	// adjust inputs: rearrange menPrefs to list women in order of preference
	menPrefs = rearrangeInOrderOfPreference(menPrefs)

	n := len(menPrefs)
	result := stdext.MakeIntSlice(n, -1)
	reverseResult := stdext.MakeIntSlice(n, -1)

	freeMen := ds.NewIntDequeFromRange(0, n)
	nextToPropose := stdext.MakeIntSlice(n, 0) // man -> woman to propose

	for freeMen.Size() > 0 {
		m := freeMen.PopFront()
		engaged := false

		for {
			w := menPrefs[m][nextToPropose[m]]
			nextToPropose[m]++
			fmt.Printf("man %d proposed to woman %d\n", m+1, w+1)

			if reverseResult[w] == -1 { // if it is a free woman
				engaged = true
			} else {
				m1 := reverseResult[w]                    // current w's partner
				if womenPrefs[w][m] < womenPrefs[w][m1] { // lesser rank position means more preferable
					result[m1] = -1 // abandon current partner
					freeMen.PushFront(m1)
					fmt.Printf("man %d becomes free\n", m1+1)

					engaged = true
				}
			}

			if engaged {
				result[m] = w
				reverseResult[w] = m
				fmt.Printf("%d and %d became engaged\n", m+1, w+1)
				break
			}
		}
	}

	// adjust output: add one for readablity
	for i := range result {
		result[i]++
	}

	return result
}

// A helper function to rearrange items in order of rank. For example,
// item 2 (counting from zero) has the highest 1st rank, so it became the first
// element in the resulting array:
// [5 3 1 2 4] -> [2 3 1 4 0]
func rearrangeInOrderOfPreference(prefs [][]int) [][]int {
	n := len(prefs)
	result := make([][]int, n)

	for row := range prefs {
		result[row] = make([]int, n)

		for col, rank := range prefs[row] {
			result[row][rank-1] = col
		}
	}

	return result
}
