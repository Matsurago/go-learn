package graph

import "gitlab.com/Matsurago/go-learn/stdext"

/*
UnionFind structure is used to find if there is a valid path between two nodes in a graph.
Connected nodes form a component. A path exists between two nodes if they belong to the same component.

Two operations are supported:
- Find: query if two nodes are connected
- Union: merge two disconnected components into one

Nodes are represented as an array of ids.
Id[i] = j  <==>  j is the parent node of i
A parent of a root is the root node itself (Id[i] = i for a root).

Note: this structure is also known as 'disjoint-sets'.
*/
type UnionFind struct {
	Id []int
}

func (u *UnionFind) Union(i, j int) {
	ri, hi := u.root(i)
	rj, hj := u.root(j)

	// append a lower tree to a higher one (not vice versa!)
	if hi < hj {
		u.Id[ri] = rj
	} else {
		u.Id[rj] = ri
	}
}

// Find determines if two nodes are connected by a path (i.e. they are in the same component)
// O(lg N) if trees are balanced
func (u *UnionFind) Find(i, j int) bool {
	ri, _ := u.root(i)
	rj, _ := u.root(j)

	return ri == rj
}

func (u *UnionFind) root(i int) (int, int) {
	height := 1

	for u.Id[i] != i {
		// path compression
		u.Id[i] = u.Id[u.Id[i]]

		i = u.Id[i]
		height += 1
	}

	return i, height
}

func makeUnionFind(n int, pairs []stdext.IntPair) UnionFind {
	u := UnionFind{
		Id: make([]int, n),
	}

	// initialize each node to its own component
	for i := range u.Id {
		u.Id[i] = i
	}

	for _, p := range pairs {
		u.Union(p.First, p.Second)
	}

	return u
}
