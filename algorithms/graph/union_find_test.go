package graph

import (
	"gitlab.com/Matsurago/go-learn/stdext"
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestUnionFind(t *testing.T) {
	pairs := []stdext.IntPair{
		{4, 3}, {3, 8}, {6, 5}, {9, 4},
		{2, 1}, {8, 9}, {5, 0}, {7, 2},
		{6, 1},
	}

	u := makeUnionFind(10, pairs)
	True(t, u.Find(0, 7))
	True(t, u.Find(5, 2))
	True(t, u.Find(3, 9))
	True(t, u.Find(8, 9))

	False(t, u.Find(0, 8))
	False(t, u.Find(6, 4))
}
