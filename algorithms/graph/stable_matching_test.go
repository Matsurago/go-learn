package graph

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/Matsurago/go-learn/algorithms/numeric"
	"gitlab.com/Matsurago/go-learn/stdext"

	. "github.com/stretchr/testify/assert"
)

func TestStableMatchingTrivialCase(t *testing.T) {
	menPrefs := [][]int{{1}}
	womenPrefs := [][]int{{1}}

	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[1]", fmt.Sprintf("%v", result))
}

func TestStableMatchingTwoPairsMeshNoConflict(t *testing.T) {
	menPrefs := [][]int{
		{2, 1},
		{1, 2},
	}
	womenPrefs := [][]int{
		{2, 1},
		{1, 2},
	}

	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[2 1]", fmt.Sprintf("%v", result))
}

func TestStableMatchingTwoPairsMenMeshWomenConflict(t *testing.T) {
	menPrefs := [][]int{
		{1, 2}, // m1 prefers w1
		{2, 1}, // m2 prefers w2: no conflict
	}
	womenPrefs := [][]int{
		{2, 1}, // w1 prefers m2
		{1, 2}, // w2 prefers m1: prefs are opposite of men
	}
	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[1 2]", fmt.Sprintf("%v", result)) // algorithm prioritizes men preferences
}

func TestStableMatchingTwoPairsConflict(t *testing.T) {
	menPrefs := [][]int{
		{1, 2}, // both men like the first woman
		{1, 2},
	}
	womenPrefs := [][]int{
		{2, 1}, // both women like the second man
		{2, 1},
	}

	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[2 1]", fmt.Sprintf("%v", result))
}

func TestStableMatchingTwoPairsConflictWoman2(t *testing.T) {
	menPrefs := [][]int{
		{2, 1}, // both men like the second woman
		{2, 1},
	}
	womenPrefs := [][]int{
		{2, 1}, // both women like the second man
		{2, 1},
	}

	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[1 2]", fmt.Sprintf("%v", result))
}

func TestStableMatchingThreePairs(t *testing.T) {
	menPrefs := [][]int{
		{1, 2, 3},
		{2, 1, 3},
		{1, 2, 3},
	}
	womenPrefs := [][]int{
		{2, 1, 3},
		{1, 2, 3},
		{1, 2, 3},
	}

	result := StableMatching(menPrefs, womenPrefs)

	Equal(t, "[1 2 3]", fmt.Sprintf("%v", result))
}

func TestStableMatchingManyRandomPairs(t *testing.T) {
	const n = 5
	menPrefs := stdext.MakeIntMatrix(n, n, 0)
	womenPrefs := stdext.MakeIntMatrix(n, n, 0)

	g := numeric.NewKnuthLCG(uint64(time.Now().UnixNano()))

	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			menPrefs[i][j] = j + 1
			womenPrefs[i][j] = j + 1
		}

		numeric.RandomShuffle(g, menPrefs[i])
		numeric.RandomShuffle(g, womenPrefs[i])
	}

	result := StableMatching(menPrefs, womenPrefs)

	fmt.Printf("Men  : %v\n", menPrefs)
	fmt.Printf("Women: %v\n", womenPrefs)
	fmt.Printf("Matching: %v\n", result)

	// adjust result
	for i := range result {
		result[i]--
	}

	// calculate reverse result: which woman is paired with which man
	reverseResult := make([]int, n)
	for m, w := range result {
		reverseResult[w] = m
	}

	// verify stability
	for m, w := range result {
		rank := menPrefs[m][w] // rank of the final partner of m

		for w1, r := range menPrefs[m] {
			// if there is a women w' which is preferable to w for m
			if r < rank {
				// if m is also preferable for w' rather than her current partner
				w1mr := womenPrefs[w1][m]
				w1pr := womenPrefs[w1][reverseResult[w1]]
				if w1mr < w1pr {
					fmt.Printf("Man %d and woman %d form an unstable pair.\n", m, w1)
					fmt.Printf("Man's %d current partner ranks %d, but woman's %d rank is %d.\n", m, rank, w1, r)
					fmt.Printf("Also, woman's %d current partner ranks %d, but man's %d rank is %d.\n", w1, w1pr, m, w1mr)
					fmt.Printf("----\n")
					t.Fail()
				}
			}
		}
	}
}
