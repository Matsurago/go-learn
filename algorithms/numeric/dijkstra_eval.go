package numeric

import (
	"gitlab.com/Matsurago/go-learn/algorithms/ds"
	"strconv"
)

// DijkstraEval is the Dijkstra algorithm to evaluate a fully parenthesized expression.
func DijkstraEval(expr string) int {
	tokens := tokenize(expr)

	// two stacks: one for values, another is for operators
	values := ds.NewStack[int]()
	ops := ds.NewStack[string]()

	for _, token := range tokens {
		switch token {
		case "(": // just skip open parenthesis
			continue
		case "+", "-", "*", "/": // push operator into the operator stack
			ops.Push(token)
		case ")": // pop operator and two values from corresponding stacks, apply the operator, push the result
			op := ops.Pop()
			b, a := values.Pop(), values.Pop()

			switch op {
			case "+":
				values.Push(a + b)
			case "-":
				values.Push(a - b)
			case "*":
				values.Push(a * b)
			case "/":
				values.Push(a / b)
			}
		default: // push the value into the values stack
			i, _ := strconv.Atoi(token)
			values.Push(i)
		}
	}

	return values.Pop()
}

func tokenize(s string) []string {
	var token string
	var res []string

	for _, c := range s {
		switch c {
		case ' ', '\t', '\n':
			if token != "" {
				res = append(res, token)
				token = ""
			}
		case '(', ')', '+', '-', '*', '/':
			if token != "" {
				res = append(res, token)
				token = ""
			}
			res = append(res, string(c))
		default:
			token += string(c)
		}
	}

	if token != "" {
		res = append(res, token)
	}

	return res
}
