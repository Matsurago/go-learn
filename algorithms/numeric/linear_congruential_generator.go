package numeric

// LCG is a linear congruential generator
type LCG struct {
	a, b, m uint64
	x       uint64
}

// NewLCG creates a new linear congruential generator
func NewLCG(a, b, m uint64, seed uint64) *LCG {
	return &LCG{a, b, m, seed}
}

// NewKnuthLCG creates a new linear congruential generator with parameters from Numerical Recipes book
func NewKnuthLCG(seed uint64) *LCG {
	return &LCG{1664525, 1013904223, 4294967296, seed}
}

// Next generates a new pseudo-random number
func (g *LCG) Next() uint64 {
	g.x = (g.a*g.x + g.b) % g.m
	return g.x
}

// NextInRange generates a new pseudo-random number in the given range
func (g *LCG) NextInRange(startInclusive, endExclusive int) int {
	scaledResult := (float64(g.Next()) / float64(g.m)) * float64(endExclusive-startInclusive)
	return startInclusive + int(scaledResult)
}
