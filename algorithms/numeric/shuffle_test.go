package numeric

import (
	"testing"
	"time"

	. "github.com/stretchr/testify/assert"
)

func TestRandomShuffle(t *testing.T) {
	g := NewKnuthLCG(uint64(time.Now().UnixNano()))
	s := []int{0, 1, 2, 3, 4}

	RandomShuffle(g, s)

	var found [5]bool
	for i := range s {
		found[s[i]] = true
	}

	for _, v := range found {
		True(t, v)
	}
}
