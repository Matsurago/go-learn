package numeric

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestGcdPositive(t *testing.T) {
	Equal(t, 3, gcd(9, 3))
	Equal(t, 6, gcd(18, 12))
	Equal(t, 1, gcd(47, 17))
	Equal(t, 13, gcd(82732, 23283))
}

func TestGcdZero(t *testing.T) {
	Equal(t, 9, gcd(9, 0))
	Equal(t, 9, gcd(0, 9))
}

func TestGcdNegative(t *testing.T) {
	Equal(t, 3, gcd(-9, -3))
	Equal(t, 3, gcd(-9, 3))
	Equal(t, 3, gcd(9, -3))
}
