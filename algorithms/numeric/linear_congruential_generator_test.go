package numeric

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestLCGNext(t *testing.T) {
	g := NewLCG(7, 5, 11, 0)

	EqualValues(t, 5, g.Next())
	EqualValues(t, 7, g.Next())
	EqualValues(t, 10, g.Next())
	EqualValues(t, 9, g.Next())
	EqualValues(t, 2, g.Next())
}

func TestLCGNextInRangePositive(t *testing.T) {
	g := NewKnuthLCG(0)
	m := make(map[int]int)

	for i := 0; i < 1000; i++ {
		m[g.NextInRange(5, 15)]++
	}

	Equal(t, 0, m[0])
	Equal(t, 0, m[4])
	Equal(t, 0, m[15])

	for i := 5; i < 15; i++ {
		True(t, m[i] > 0)
	}
}

func TestLCGNextInRangeNegative(t *testing.T) {
	g := NewKnuthLCG(0)
	m := make(map[int]int)

	for i := 0; i < 1000; i++ {
		m[g.NextInRange(-15, -5)]++
	}

	Equal(t, 0, m[0])
	Equal(t, 0, m[-16])
	Equal(t, 0, m[-5])

	for i := -15; i < -5; i++ {
		True(t, m[i] > 0)
	}
}
