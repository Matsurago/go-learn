package numeric

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestDijkstraEvalTokenize(t *testing.T) {
	res := tokenize("(1+( (2 + 3)*(4* 5) )) ")
	exp := []string{"(", "1", "+", "(", "(", "2", "+", "3", ")", "*", "(", "4",
		"*", "5", ")", ")", ")"}

	Equal(t, exp, res)
}

func TestDijkstraEval(t *testing.T) {
	Equal(t, 101, DijkstraEval("(1 + ((2 + 3) * (4 * 5)))"))
	Equal(t, 2, DijkstraEval("((13 - 3) / ((2 * 2) + 1))"))
}
