package ds

import (
	"fmt"
)

// Deque is a queue that allows inserting items to its both ends (front and back).
type Deque[T any] struct {
	size int
	head *node[T]
	tail *node[T]
}

type node[T any] struct {
	value T
	next  *node[T]
	prev  *node[T]
}

// NewDeque creates an empty queue.
func NewDeque[T any]() *Deque[T] {
	// sentinels (empty nodes with no values)
	// these simplify adding/removing items to queue by removing the need to do nil-checks
	tail := &node[T]{}
	head := &node[T]{
		next: tail,
	}

	tail.prev = head

	return &Deque[T]{
		size: 0,
		head: head,
		tail: tail,
	}
}

// PushBack appends a new item to the end of the queue.
func (q *Deque[T]) PushBack(value T) {
	newTail := &node[T]{
		prev: q.tail,
	}

	q.tail.value = value
	q.tail.next = newTail

	q.tail = newTail
	q.size++
}

// PushFront appends a new item to the front of the queue.
func (q *Deque[T]) PushFront(value T) {
	newHead := &node[T]{
		next: q.head,
	}

	q.head.value = value
	q.head.prev = newHead

	q.head = newHead
	q.size++
}

// PopFront retrieves the first item in queue and removes it from the queue.
func (q *Deque[T]) PopFront() T {
	if q.size == 0 {
		panic("queue is empty")
	}

	first := q.head.next
	q.head.next = first.next
	first.next.prev = q.head
	q.size--

	return first.value
}

// PopBack retrieves the last item in queue and removes it from the queue.
func (q *Deque[T]) PopBack() T {
	if q.size == 0 {
		panic("queue is empty")
	}

	last := q.tail.prev
	q.tail.prev = last.prev
	last.prev.next = q.tail
	q.size--

	return last.value
}

// First retrieves the first item in queue without removing it from the queue.
func (q *Deque[T]) First() T {
	if q.size == 0 {
		panic("queue is empty")
	}

	return q.head.next.value
}

// Last retrieves the last item in queue without removing it from the queue.
func (q *Deque[T]) Last() T {
	if q.size == 0 {
		panic("queue is empty")
	}

	return q.tail.prev.value
}

// Size returns the number of items in the queue.
func (q *Deque[T]) Size() int {
	return q.size
}

func (q *Deque[T]) String() string {
	if q.size == 0 {
		return "[]"
	}

	result := fmt.Sprintf("[%v", q.First())
	p := q.head.next.next // second element or tail sentinel

	for p.next != nil {
		result += fmt.Sprintf(" %v", p.value)
		p = p.next
	}

	result += "]"
	return result
}

type IntDeque = Deque[int]

func NewIntDeque() *Deque[int] {
	return NewDeque[int]()
}

func NewIntDequeFromRange(from, to int) *IntDeque {
	q := NewIntDeque()
	for i := from; i < to; i++ {
		q.PushBack(i)
	}
	return q
}
