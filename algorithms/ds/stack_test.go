package ds

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestStack(t *testing.T) {
	s := NewStack[int]()

	s.Push(1)
	s.Push(2)
	s.Push(3)
	Equal(t, 3, s.Pop())

	s.Push(4)
	s.Push(5)

	Equal(t, 5, s.Pop())
	Equal(t, 4, s.Pop())
	Equal(t, 2, s.Pop())
	Equal(t, 1, s.Pop())
}
