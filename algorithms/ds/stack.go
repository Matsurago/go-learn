package ds

type Stack[T any] struct {
	items []T
	pos   int
}

func NewStack[T any]() *Stack[T] {
	return &Stack[T]{}
}

func (s *Stack[T]) Push(val T) {
	if s.pos < len(s.items) {
		s.items[s.pos] = val
	} else {
		s.items = append(s.items, val)
	}
	s.pos++
}

func (s *Stack[T]) Pop() T {
	if s.pos == 0 {
		panic("the stack is empty!")
	}

	s.pos--
	return s.items[s.pos]
}

func (s *Stack[T]) IsEmpty() bool {
	return s.pos == 0
}
