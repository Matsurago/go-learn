package ds

import (
	"testing"

	. "github.com/stretchr/testify/assert"
)

func TestEmptyDeque(t *testing.T) {
	q := NewDeque[string]()

	Equal(t, "[]", q.String())
}

func TestSingleElementDequePushBack(t *testing.T) {
	q := NewDeque[int]()
	q.PushBack(2)

	Equal(t, 2, q.First())
	Equal(t, 2, q.Last())
	Equal(t, "[2]", q.String())
}

func TestSingleElementDequePushFront(t *testing.T) {
	q := NewIntDeque()
	q.PushFront(2)

	Equal(t, 2, q.First())
	Equal(t, 2, q.Last())
	Equal(t, "[2]", q.String())
}

func TestTwoElementDequePushBack(t *testing.T) {
	q := NewIntDeque()
	q.PushBack(2)
	q.PushBack(3)

	Equal(t, 2, q.First())
	Equal(t, 3, q.Last())
	Equal(t, "[2 3]", q.String())
}

func TestTwoElementDequePushFront(t *testing.T) {
	q := NewIntDeque()
	q.PushFront(2)
	q.PushFront(3)

	Equal(t, 3, q.First())
	Equal(t, 2, q.Last())
	Equal(t, "[3 2]", q.String())
}

func TestTwoElementDequePushFrontAndBack(t *testing.T) {
	q := NewIntDeque()
	q.PushFront(2)
	q.PushBack(3)

	Equal(t, 2, q.First())
	Equal(t, 3, q.Last())
	Equal(t, "[2 3]", q.String())
}

func TestTwoElementDequePushBackAndFront(t *testing.T) {
	q := NewIntDeque()
	q.PushBack(2)
	q.PushFront(3)

	Equal(t, 3, q.First())
	Equal(t, 2, q.Last())
	Equal(t, "[3 2]", q.String())
}

func TestDequePop(t *testing.T) {
	q := NewIntDeque()
	q.PushBack(2)
	q.PushBack(3)

	q.PopFront()
	Equal(t, 1, q.Size())
	Equal(t, "[3]", q.String())

	q.PopFront()
	Equal(t, 0, q.Size())
	Equal(t, "[]", q.String())

	q.PushBack(4)
	Equal(t, 1, q.Size())
	Equal(t, "[4]", q.String())

	q.PushBack(5)
	Equal(t, 2, q.Size())
	Equal(t, "[4 5]", q.String())
}

func TestDequePopPushBackAndFront(t *testing.T) {
	q := NewIntDeque()
	q.PushBack(2)
	q.PushBack(3)
	q.PushFront(4)
	q.PushFront(5)

	Equal(t, 5, q.PopFront())
	Equal(t, "[4 2 3]", q.String())

	q.PushFront(6)
	Equal(t, "[6 4 2 3]", q.String())

	Equal(t, 3, q.PopBack())
	Equal(t, "[6 4 2]", q.String())

	Equal(t, 6, q.PopFront())
	Equal(t, "[4 2]", q.String())

	Equal(t, 2, q.PopBack())
	Equal(t, "[4]", q.String())

	Equal(t, 4, q.PopBack())
	Equal(t, "[]", q.String())
}
