# go-snippets
Go programming language code snippets (with applications).

## Contents
- algorithms: classic Computer Science algorithms implemented in Go
- apps: sample apps in Go
- database: demo on how to work with a database
- snippets: short code snippets to demonstrate Go syntax
- stdext: utility functions

## Running Tests
To run all tests in the repository, execute:
```
go test -v ./...
```
You have to comment out the test `snippets/channels/safety/deadlock_test.go`.

To run only tests that have a word XX in their names, execute
```
go test -run="XX"
```
Note that single quotes won't work.

## Useful Commands
Add a dependency, automatically modifies the `go.mod` file:
```
go get <dependency_name>
```

Update all dependencies:
```
go get -u ./...
```

Build an app with a race detector:
```
go build -race
```

Perform escape analysis to see when memory is allocated on heap:
```
go build -gcflags="-m" <filename.go>
```

## Notes About Go
This is a badly designed programming language. The arrogance of the guys
behind it was unimaginable, but history put them into the right place. That
arrogance was also the reason to put tabs when all languages use spaces;
use 'funny' (not) constants everyone must remember for date formatting;
the refusal to initially provide generics ("we don't need them"), and later
trying to back out of that stance with excuses. They ignored much of the
progress done in other programming languages, so when Go came out it looked
like a thing from 70ties. 

The lack of generics made it impossible to write functions in the standard library
that everyone needs. For example, it took them 15 years to add a function
that tests if a value is contained in a list (or 'slice' in Go terminology).

They've done one thing right though, and this is the concurrency aspect.
That's why this language still lives despite all it flaws (backing from
Google helps too).

My personal list of bad things in Go, in no particular order:
- Forced tabs
- Unreadable one-letter variable names and other abbreviations are preferred
- Unused import is an error
- Unused variable is an error
- No numeric type conversion even when it's safe
- Case in function names defines the function scope
- Worst date formatting rules ever
- Can return tuples from functions, but can't use them elsewhere
- No default parameters for functions
- GitHub URLs in imports
- Need to use the `make` function for collections
- Bug-prone `append` function use pattern
- Ugly map type declaration: e.g. `map[string][]string` 
- Tedious and verbose error handling
- No built-in test assertions
- Magic in the language that cannot be replicated
- Revived `goto`
- etc.
