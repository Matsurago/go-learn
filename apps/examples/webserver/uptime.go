package main

import (
	"fmt"
	"net/http"
	"time"
)

type UptimeHandler struct {
	Started time.Time
}

func NewUptimeHandler() UptimeHandler {
	return UptimeHandler{
		Started: time.Now(),
	}
}

// must implement this method in http.Handler interface
func (h UptimeHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	uptime := time.Since(h.Started)

	fmt.Fprintf(writer, "Uptime: %v\n", uptime)
}
