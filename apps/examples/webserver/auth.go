package main

import "net/http"

type AuthHandler struct {
	Username string
	Next     *http.ServeMux
}

// must implement this method in http.Handler interface
func (h AuthHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	username := request.URL.Query().Get("u") // get a query parameter

	if username != h.Username {
		http.Error(writer, "Access Denied", http.StatusForbidden)
		return
	}

	h.Next.ServeHTTP(writer, request) // pass execution to the next handler in the chain
}
