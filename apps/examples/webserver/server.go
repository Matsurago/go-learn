package main

import (
	"flag"
	"fmt"
	"net/http"
)

func main() {
	port := flag.Int("p", 3000, "Port number to listen for requests")
	flag.Parse()

	// all requests must be authorized
	// NOTE: '/' matches all requests, not only root
	http.Handle("/", AuthHandler{
		Username: "user",
		Next:     RegisterEndpoints(),
	})

	address := fmt.Sprintf(":%d", *port)
	fmt.Printf("Starting server on localhost:%v\n", address)

	// need 2nd arg to be nil for http.Handle() and http.HandleFunc() to work
	err := http.ListenAndServe(address, nil)
	if err != nil {
		fmt.Printf("[ERROR] Unable to start the server:\n%v\n", err.Error())
	}
}
