package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
)

// load all templates from /templates folder
var templates = initTemplates()

func initTemplates() *template.Template {
	// template.Must() panics if there is a problem loading a template
	return template.Must(template.New("t").ParseGlob("templates/*.html"))
}

func RegisterEndpoints() *http.ServeMux {
	// request multiplexer
	var mux http.ServeMux

	// register endpoints using a function
	mux.HandleFunc("/users", usersEndpoint)
	mux.HandleFunc("/posts/", postsEndpoint) // entire subtree, requests like /posts/latest are valid

	mux.HandleFunc("/error", errorEndpoint)
	mux.HandleFunc("/redirect", redirectEndpoint)

	// use a handler struct
	mux.Handle("/uptime", NewUptimeHandler())

	// IMPORTANT: default router directs any request that it does not have a handler for to /
	mux.HandleFunc("/", rootEndpoint)

	return &mux
}

func rootEndpoint(writer http.ResponseWriter, request *http.Request) {
	// add a header
	writer.Header().Add("Server", "Simple HTTP Server")

	if request.URL.Path != "/" {
		// we are redirected from non-registered endpoint
		// should return 404 instead of rendering the index page
		http.NotFound(writer, request)
		return
	}

	// render Welcome template (/templates/welcome.html)
	renderTemplate(writer, request, "welcome", "Guest")
}

func usersEndpoint(writer http.ResponseWriter, _ *http.Request) {
	users := []struct {
		Id    int    `json:"id"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}{
		{100, "John Ford", "john.ford@vector.com"},
		{101, "Michael Hopkins", "mich.hopkins@vector.com"},
		{102, "Julia Silvester", "just.julia@vector.com"},
	}

	err := json.NewEncoder(writer).Encode(users)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

func postsEndpoint(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(writer, "You accessed: /posts")
}

// sending custom error code
func errorEndpoint(writer http.ResponseWriter, request *http.Request) {
	http.Error(writer, "Server was unable to process the request", 500)
}

// redirect example
func redirectEndpoint(writer http.ResponseWriter, request *http.Request) {
	http.Redirect(writer, request, "https://ya.ru", http.StatusMovedPermanently)
}

func renderTemplate(writer http.ResponseWriter, request *http.Request, name string, data interface{}) {
	err := templates.ExecuteTemplate(writer, name, data)

	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}
}
