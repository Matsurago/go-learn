# Escape Analysis Example

## Run
```
go build -gcflags="-m" escape.go
```

## Expected Output
```
./escape.go:8:6: can inline countdown
./escape.go:21:14: inlining call to fmt.Println
./escape.go:8:16: i does not escape
./escape.go:16:2: moved to heap: count
./escape.go:21:14: ... argument does not escape
./escape.go:21:15: count escapes to heap
```
The important part is
```
./escape.go:16:2: moved to heap: count
```
