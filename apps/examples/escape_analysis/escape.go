package main

import (
	"fmt"
	"time"
)

func countdown(i *int) {
	for *i > 0 {
		time.Sleep(1 * time.Second)
		*i -= 1
	}
}

func main() {
	count := 5
	go countdown(&count)

	for count > 0 {
		time.Sleep(500 * time.Millisecond)
		fmt.Println(count) // race condition: can print 0
	}
}
