package main

import (
	_ "embed"
	"fmt"
)

//go:embed data/LICENSE
var license string

func main() {
	fmt.Println(license)
}
