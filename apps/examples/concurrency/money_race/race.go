package main

import (
	"fmt"
	"sync"
)

const N = 1_000_000

func earn(account *int, wg *sync.WaitGroup) {
	for i := 0; i < N; i++ {
		*account += 10
	}
	wg.Done()
}

func spend(account *int, wg *sync.WaitGroup) {
	for i := 0; i < N; i++ {
		*account -= 10
	}
	wg.Done()
}

func main() {
	account := 100

	var wg sync.WaitGroup
	wg.Add(2)

	go earn(&account, &wg)
	go spend(&account, &wg)

	wg.Wait()
	fmt.Printf("Balance = %d", account)
}
