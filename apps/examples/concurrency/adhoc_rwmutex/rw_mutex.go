// Ad-hoc implementation of a Read/Write mutex, optimized for readers
// only for illustrative purposes; use sync.RWMutex instead
package main

import (
	"fmt"
	"sync"
	"time"
)

// ReadWriteMutex allows concurrent reader access, but only one goroutine can write at a time.
// optimized for readers: can't write when any goroutine is reading
// also blocks all read access when someone's writing
type ReadWriteMutex struct {
	readersCount     int
	readersCountLock sync.Mutex
	writerLock       sync.Mutex
}

func (mu *ReadWriteMutex) ReaderLock() {
	mu.readersCountLock.Lock()
	mu.readersCount++
	if mu.readersCount == 1 {
		mu.writerLock.Lock()
	}
	mu.readersCountLock.Unlock()
}

func (mu *ReadWriteMutex) WriterLock() {
	mu.writerLock.Lock()
}

func (mu *ReadWriteMutex) ReaderUnlock() {
	mu.readersCountLock.Lock()
	mu.readersCount--
	if mu.readersCount == 0 {
		mu.writerLock.Unlock()
	}
	mu.readersCountLock.Unlock()
}

func (mu *ReadWriteMutex) WriterUnlock() {
	mu.writerLock.Unlock()
}

func main() {
	// many read goroutines may constantly hog on the mutex, so that write goroutines won't be able to do their work
	// => read goroutines are starving write goroutines
	const numEventsInitial = 1000
	const numWrites = 20
	const numReads = 200
	var wg sync.WaitGroup

	var mu ReadWriteMutex

	var events []string
	for i := 0; i < numEventsInitial; i++ {
		events = append(events, fmt.Sprintf("Event #%d", i+1))
	}

	for i := 0; i < numWrites; i++ {
		wg.Add(1)
		i := i

		go func(i int, mu *ReadWriteMutex, wg *sync.WaitGroup) {
			mu.WriterLock()

			fmt.Printf("writing %d\n", i)
			events = append(events, fmt.Sprintf("Event #%d", i+numEventsInitial+1))

			mu.WriterUnlock()

			time.Sleep(200 * time.Millisecond)
			wg.Done()
		}(i, &mu, &wg)
	}

	for i := 0; i < numReads; i++ {
		wg.Add(1)
		i := i

		go func(i int, mu *ReadWriteMutex, wg *sync.WaitGroup) {
			mu.ReaderLock()

			fmt.Printf("reading %d\n", i)
			var eventCopy []string
			for i := 0; i < len(events); i++ {
				eventCopy = append(eventCopy, events[i])
			}
			fmt.Printf("reading %d done: %d events\n", i, len(eventCopy))

			mu.ReaderUnlock()

			time.Sleep(100 * time.Millisecond)
			wg.Done()
		}(i, &mu, &wg)
	}

	wg.Wait()
}
