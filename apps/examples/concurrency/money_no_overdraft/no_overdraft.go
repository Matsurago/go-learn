// Solves the race condition in money_race.go
// Additionally, uses condition variables to prevent withdrawing from a bank account
// when there are not enough funds.
package main

import (
	"fmt"
	"sync"
)

const (
	earnOps     = 1_000_000
	earnAmount  = 10
	spendOps    = 200_000
	spendAmount = 50
)

func earn(account *int, cond *sync.Cond, wg *sync.WaitGroup) {
	for i := 0; i < earnOps; i++ {
		cond.L.Lock() // important: use mutex inside the condition
		*account += earnAmount
		cond.Signal() // signal a change that may make the condition true (but not necessary: we're waiting for $50)
		// NOTE: use Signal() to notify one waiting goroutine; use Broadcast() to notify all of them.
		cond.L.Unlock()
	}
	wg.Done()
}

func spend(account *int, cond *sync.Cond, wg *sync.WaitGroup) {
	for i := 0; i < spendOps; i++ {
		cond.L.Lock()
		// this is a more robust replacement for time.Sleep()
		for *account < spendAmount { // wait infinitely until we have enough money, wake up on each Signal()
			cond.Wait() // atomically suspends the execution of this goroutine AND releases the mutex
		}
		// return from Wait() reacquires the mutex
		// NOTE: always call Wait(), Signal(), and Broadcast() when holding the mutex of the condition.
		// NOTE: calling Signal() when Wait() is not started yet results in a missed signal,
		//       so that Wait() never receives it (can lead to deadlock)
		*account -= spendAmount
		cond.L.Unlock()
	}
	wg.Done()
}

func main() {
	account := 100

	var wg sync.WaitGroup
	wg.Add(2)

	var mu sync.Mutex
	cond := sync.NewCond(&mu) // condition variable

	go earn(&account, cond, &wg)
	go spend(&account, cond, &wg)

	// wait earn & spend to finish
	wg.Wait()

	// final balance should equal the initial balance
	mu.Lock()
	fmt.Printf("Balance = %d", account)
	mu.Unlock()
}
