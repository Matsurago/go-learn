package main

import (
	"fmt"
	"io"
	"net/http"
)

type MyHandler struct{}

func (h *MyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	res := fmt.Sprintf("Secure request for %v", req.URL.Path)

	_, err := io.WriteString(w, res)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	http.Handle("/message", &MyHandler{})
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.Handle("/", http.RedirectHandler("/message", http.StatusTemporaryRedirect))

	err := http.ListenAndServeTLS(":5000", "cert.pem", "key.pem", nil)
	if err != nil {
		fmt.Println(err)
	}
}
