// A gif image creation example from "The Go Programming Language" book, but refactored.
// A good example how working with numbers is tedious in Go due to numeric type conversions.
// Usage: go run lissajous.go > image.gif
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

func main() {
	lissajous(os.Stdout, 128, 64, 8, 5, 1e-3)
}

// size: [-size..size]
func lissajous(w io.Writer, size int, numFrames int, delay int, numCycles int, angularRes float64) {
	freq := rand.Float64() * 3.0
	phase := 0.0

	anim := gif.GIF{LoopCount: numFrames}
	palette := []color.Color{
		color.Black,
		color.RGBA{R: 0, G: 0xff, B: 0, A: 0xff}, // green
	}

	for i := 0; i < numFrames; i++ {
		img := image.NewPaletted(
			image.Rect(0, 0, 2*size+1, 2*size+1),
			palette,
		)

		for t := 0.0; t < 2*math.Pi*float64(numCycles); t += angularRes {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)

			imgX := size + int(x*float64(size)+0.5)
			imgY := size + int(y*float64(size)+0.5)
			img.SetColorIndex(imgX, imgY, 1) // 1 = index of green color in palette
		}

		phase += 0.1

		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}

	err := gif.EncodeAll(w, &anim)
	if err != nil {
		//goland:noinspection GoUnhandledErrorResult
		fmt.Fprintf(os.Stderr, "%v", err)
	}
}
