// Spinner example from "The Go Programming Language" book.
// Displays a spinner while a long computation is in progress.
package main

import (
	"fmt"
	"time"
)

func fib(n int) int {
	if n < 2 {
		return n
	}

	return fib(n-1) + fib(n-2)
}

func spinner(delay time.Duration) {
	for {
		for _, c := range `-\|/` {
			fmt.Printf("\r%c", c)
			time.Sleep(delay)
		}
	}
}

func main() {
	const n = 45

	go spinner(100 * time.Millisecond)
	res := fib(n)
	fmt.Printf("\rfib(%d) = %d\n", n, res)
}
