package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"sort"
)

func main() {
	err := http.ListenAndServe(":5000", &MyHandler{})

	if err != nil {
		fmt.Println(err)
	}
}

type MyHandler struct{}

//goland:noinspection GoUnhandledErrorResult
func (h *MyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	buf := &bytes.Buffer{}

	fmt.Fprintln(buf, "<h1>Request Details</h1>")
	fmt.Fprintf(buf, "<p>%v %v %v</p>", req.Method, req.Proto, req.URL.Path)

	fmt.Fprintln(buf, "<ul>")
	for _, header := range getKeys(req.Header) {
		value := req.Header[header]

		if len(value) == 1 {
			fmt.Fprintf(buf, "<li><b>%v</b>: %v</li>", header, value[0])
		} else {
			fmt.Fprintf(buf, "<li><b>%v</b>: <ul>", header)
			for _, v := range value {
				fmt.Fprintf(buf, "<li>%v</li>", v)
			}
			fmt.Printf("</ul></li>")
		}
	}
	fmt.Fprintln(buf, "</ul>")

	fmt.Fprintf(buf, "<h2>Contents:</h2>%v", req.Body)

	fmt.Fprintln(buf, "<h2>Cookies:</h2><ul>")
	for _, cookie := range req.Cookies() {
		fmt.Fprintf(buf, "<li><b>%v<b>: %v</li>", cookie.Name, cookie.Value)
	}
	fmt.Fprintln(buf, "</ul>")

	_, err := io.WriteString(w, buf.String())
	if err != nil {
		fmt.Println(err.Error())
	}
}

func getKeys[T any](m map[string]T) []string {
	var keys []string
	for k, _ := range m {
		keys = append(keys, k)
	}

	sort.Strings(keys)
	return keys
}
