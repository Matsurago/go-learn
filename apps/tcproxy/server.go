package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	port := flag.Int("p", 80, "port")
	flag.Parse()

	startServer(*port)
}

func startServer(port int) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))

	if err != nil {
		log.Fatalf("Unable to bind to port %d (%v).\n", port, err)
	}
	log.Printf("Listening on 0.0.0.0:%d\n", port)

	for {
		conn, err := listener.Accept()

		if err != nil {
			log.Fatalf("Unable to accept connection (%s)\n", err)
		}

		go handleConnection(conn)
	}
}

func handleConnection(source net.Conn) {
	const TargetHost string = "google.com"
	destination, err := net.Dial("tcp", fmt.Sprintf("%s:80", TargetHost))

	if err != nil {
		log.Fatalf("Unable to connect to '%s': %v", TargetHost, err)
	}

	defer destination.Close()

	// start a coroutine to avoid blocking by copy()
	go func() {
		// copy source input to destination
		if _, err := io.Copy(destination, source); err != nil {
			log.Fatalln("Unable to copy source input to destination.")
		}
	}()

	if _, err := io.Copy(source, destination); err != nil {
		log.Fatalln("Unable to copy destination output back to source.")
	}
}
