package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"slices"
	"strings"
)

const serverAddress = ":23"

type Player struct {
	name string
	conn chan string
}

func main() {
	serverSocket, err := net.Listen("tcp", serverAddress)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Server is started on %s, use a Telnet client to connect.", serverAddress)

	players := make(chan Player)
	go matchmaker(players)

	for {
		socket, err := serverSocket.Accept()
		if err != nil {
			log.Print(err)
			continue
		}

		go handleClient(socket, players)
	}
}

func handleClient(socket net.Conn, players chan Player) {
	log.Printf("Client '%s' connected.\n", getClientId(socket))

	send(socket, "Enter your nickname: ")
	name := receive(socket)

	conn := make(chan string)

	players <- Player{
		name: name,
		conn: conn,
	}

	send(socket, "Finding match...\r\n")
	opponentName := <-conn
	send(socket, fmt.Sprintf("Match found. Your opponent is %s!\r\n", opponentName))

	conn <- readChoice(socket)

	send(socket, fmt.Sprintf("Waiting for %s...\r\n", opponentName))
	opponentChoice := <-conn
	send(socket, fmt.Sprintf("Your opponent chooses %s!\r\n", opponentChoice))

	gameResult := <-conn
	send(socket, gameResult+"\r\n")

	err := socket.Close()
	if err != nil {
		log.Printf("[WARN] Cannot close connection to client '%s'", getClientId(socket))
	}
	log.Printf("Client '%s' disconnected.\n", getClientId(socket))
}

func readChoice(socket net.Conn) string {
	validChoices := []string{
		"ROCK", "SCISSORS", "PAPER",
	}

	for {
		send(socket, "Choose ROCK, SCISSORS, or PAPER: ")
		choice := receive(socket)
		choice = strings.ToUpper(choice)

		if slices.Contains(validChoices, choice) {
			return choice
		}

		send(socket, "Invalid choice, try again.\r\n")
	}
}

func getClientId(socket net.Conn) net.Addr {
	return socket.RemoteAddr()
}

func send(socket net.Conn, data string) {
	writer := bufio.NewWriter(socket)

	_, err := writer.WriteString(data)
	if err != nil {
		log.Printf("[WARN] Cannot send data '%s' to client '%s'", data, getClientId(socket))
	} else {
		err := writer.Flush()
		if err != nil {
			log.Printf("[WARN] Cannot finish sending data to client '%s'", getClientId(socket))
		}
	}
}

func receive(socket net.Conn) string {
	reader := bufio.NewReader(socket)
	var buf bytes.Buffer

	for {
		data, hasMore, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Printf("[WARN] Cannot read data from client '%s'", socket.LocalAddr())
		}

		buf.Write(data)

		if !hasMore {
			break
		}
	}

	return buf.String()
}

func game(choice1 string, choice2 string) (string, string) {
	beats := map[string]string{
		"PAPER":    "ROCK",
		"ROCK":     "SCISSORS",
		"SCISSORS": "PAPER",
	}

	if choice1 == choice2 {
		return "TIE", "TIE"
	}

	if beats[choice1] == choice2 {
		return "YOU WIN!", "YOU LOSE!"
	} else {
		return "YOU LOSE!", "YOU WIN!"
	}
}

func matchmaker(players chan Player) {
	for {
		player1 := <-players
		player2 := <-players

		go playMatch(player1, player2)
	}
}

func playMatch(player1 Player, player2 Player) {
	player1.conn <- player2.name
	player2.conn <- player1.name

	choice1 := strings.ToUpper(<-player1.conn)
	choice2 := strings.ToUpper(<-player2.conn)
	player1.conn <- choice2
	player2.conn <- choice1

	result1, result2 := game(choice1, choice2)
	player1.conn <- result1
	player2.conn <- result2
}
