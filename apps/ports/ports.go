package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
)

// Address is a simple Server/Port pair structure.
type Address struct {
	Server string
	Port   int
}

var emptyAddress = Address{}

func main() {
	targetHost := flag.String("h", "localhost", "hostname to scan") // "scanme.nmap.org"
	portRange := flag.String("p", "1-1024", "ports to scan")
	numThreads := flag.Int("n", 100, "number of parallel connections")

	flag.Parse()
	portStart, portEnd := parsePortRange(*portRange)

	openPorts := scan(*targetHost, portStart, portEnd, *numThreads)

	printResults(openPorts)
}

func scan(targetHost string, portStart int, portEnd int, numThreads int) []Address {
	addressesToScan := make(chan Address, numThreads)
	results := make(chan Address)

	// graceful shutdown on CTRL+C
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	for i := 1; i <= numThreads; i++ {
		go portScanner(addressesToScan, results)
	}

	go func() {
		for port := portStart; port <= portEnd; port++ {
			addressesToScan <- Address{targetHost, port}
		}
	}()

	var openPorts []Address
	for i := 0; i < (portEnd - portStart + 1); i++ {
		select {
		case <-sig:
			fmt.Println("  (Interrupted by the user.)")
			goto exit
		case result := <-results:
			if result != emptyAddress {
				openPorts = append(openPorts, result)
			}
		}
	}

	close(addressesToScan)
	close(results)

exit:
	return openPorts
}

func portScanner(addresses chan Address, results chan Address) {
	for address := range addresses {
		conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", address.Server, address.Port))

		if err == nil {
			results <- address // port is open
			conn.Close()
		} else {
			results <- emptyAddress
		}
	}
}

func printResults(results []Address) {
	sort.SliceStable(results, func(i, j int) bool {
		return results[i].Server <= results[j].Server && results[i].Port < results[j].Port
	})

	for _, address := range results {
		fmt.Printf("%s:%d OPEN\n", address.Server, address.Port)
	}
}

func parsePortRange(val string) (portStart int, portEnd int) {
	tokens := strings.Split(val, "-")

	if len(tokens) == 1 {
		portStart = parsePort(tokens[0])
		portEnd = portStart
		return
	}

	if len(tokens) != 2 || len(tokens[0]) == 0 || len(tokens[1]) == 0 {
		fmt.Printf("Incorrect port range: '%s'.\n", val)
		os.Exit(1)
	}

	portStart = parsePort(tokens[0])
	portEnd = parsePort(tokens[1])

	if portEnd < portStart {
		fmt.Printf("Incorrect port range: '%s'.\n", val)
		os.Exit(1)
	}

	return
}

func parsePort(val string) int {
	port, err := strconv.Atoi(val)

	if err != nil {
		fmt.Printf("Incorrect port: '%s'.\n", val)
		os.Exit(1)
	}

	return port
}
